# OSC encoding/decoding for C99

Work in progress!

![](logo.svg "COSC")

`cosc` is a small C99 library with low level functions to encode/decode and
manipulate OSC messages and bundles.


## Features

- MIT license.
- C99.
- Support for all OSC specification types, including the extended types.
- No dynamic memory.
- No transport layer.
- Address pattern and typetag matching/validation functions.
- Misc helpful functions like NTP time conversion.
- Option to disable stdlib.

Tested with:

- Linux x86_64 gcc 10.2.1
- Linux x86_64 clang 11.0.0
- MacOS Apple clang 11.0.0
- Windows x86_64 MinGW 10.2.1 (cross compiled on Linux)


## Caveats

- No support for arrays. I'm not exactly sure how arrays work in OSC,
  they are not described in much detail in the standard. Any help appreciated.
- The largest message, bundle and/or BLOB size is 2147483644 bytes
  (the largest, 4-byte aligned signed 32 bit integer).
- TODO: benchmark?


## Requirements

- A C99 compliant compiler.
- cmake. Optional, the two files can also just be dropped into your project
  and built using your build system of choice.
- The tools, examples and unit tests require stdlib.


## Disabling stdlib.

At compile time, define `-DCOSC_NOSTDLIB` to disable the use of stdlib.
This can be configured via cmake as well using the configuration
variable "COSC_DEFINE_NOSTDLIB".

You will also have to define `#define COSC_NOSTDLIB` before including `cosc.h`.

If stdlib is disabled you may want to define the following
types when building and before including `cosc.h`:

- uint8_t
    - `-DCOSC_UINT8=<type>` or "COSC_DEFINE_UINT8" in cmake.
    - Defaults to `unsigned char`.
- int32_t
    - `-DCOSC_INT32=<type>` or "COSC_DEFINE_INT32" in cmake.
    - Defaults to `signed int`.
- uint32_t
    - `-DCOSC_UINT32=<type>` or "COSC_DEFINE_UINT32" in cmake.
    - Defaults to `unsigned int`.
- int64_t
    - `-DCOSC_INT64=<type>` or "COSC_DEFINE_INT64" in cmake.
    - Defaults to `signed long` on Linux 64-bit.
    - Defaults to `signed long long` for all other platforms.
- uint64_t
    - `-DCOSC_UINT64=<type>` or "COSC_DEFINE_UINT64" in cmake.
    - Defaults to `unsigned long` on Linux 64-bit.
    - Defaults to `unsigned long long`.

If you don't want to do it using defines, you can simply typedef
the following types to the correct width for your system:
`uint8_t`, `int32_t`, `uint32_t`, `int64_t` and `uint64_t`.


## License

```
Copyright 2021 Peter Gebauer

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```