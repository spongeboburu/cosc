/**
 * @file cosc.h
 * @brief OSC encoding/decoding for C99.
 * @copyright Copyright 2021 Peter Gebauer (MIT license)
 *
 * Encode and decode OSC.
 *
 * @section defines Compile and include time defines
 *
 * - `-DCOSC_NOSTDLIB` at compile time to disable stdlib.
 *     - `-DCOSC_UINT8=<type>` used to typedef `uint8_t`,
 *       the default is `unsigned char`.
 *     - `-DCOSC_INT32=<type>` used to typedef `int32_t`,
 *       the default is `signed int`.
 *     - `-DCOSC_UINT32=<type>` used to typedef `uint32_t`,
 *       the default is `unsigned int`.
 *     - `-DCOSC_INT64=<type>` used to typedef `int64_t`,
 *       the default is `signed long` for Linux 64-bit and
 *       `signed long long` for all other.
 *     - `-DCOSC_UINT64=<type>` used to typedef `uint64_t`,
 *       the default is `unsigned long` for Linux 64-bit and
 *       `unsigned long long` for all other.
 * 
 *
 * @section data_types Data types
 *
 * All the standard types are supported:
 *
 * | Type | Description                                       |
 * | :--- | :---                                              |
 * | `i`  | Signed, 32-bit, two's complement integer.         |
 * | `f`  | 32-bit, IEEE 754 single precision floating point. |
 * | `s`  | A zero terminated string.                         |
 * | `b`  | An array of bytes.                                |
 * | `h`  | Signed, 64-bit, two's complement integer.         |
 * | `t`  | Unsigned, 64-bit integer (NTP timestamp).         |
 * | `d`  | 64-bit, IEEE 754 double precision floating point. |
 * | `S`  | A zero terminated string.                         |
 * | `c`  | An ASCII character.                               |
 * | `r`  | Unsigned, 32-bit integer.                         |
 * | `m`  | Four bytes of MIDI data.                          |
 * | `T`  | Boolean true value. Has no payload.               |
 * | `F`  | Boolean false value. Has no payload.              |
 * | `N`  | Nil value. Has no payload.                        |
 * | `I`  | Infinity value. Has no payload.                   |
 *
 * @section license License
 *
 * ```unparsed
 * Copyright 2021 Peter Gebauer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ```
 */

#ifndef COSC_H
#define COSC_H

/**
 * @def COSC_API
 * @brief Define attributes for external linkage.
 */

/* External linkage. */
#if _WIN32
#ifdef COSC_BUILD
#define COSC_API __declspec(dllexport)
#else
#define COSC_API
#endif
#else /* _WIN32 */
#define COSC_API
#endif /* _WIN32 */

/**
 * @brief Generic error.
 */
#define COSC_ERROR -1

/**
 * @brief Out of memory.
 */
#define COSC_ENOMEM -2

/**
 * @brief Integer overflow.
 */
#define COSC_EOVERFLOW -3

/**
 * @brief Not enough bytes to complete encoding/decoding.
 */
#define COSC_ESIZE -4

/**
 * @brief The typetag is invalid.
 */
#define COSC_ETYPETAG -5

/**
 * @brief The address is invalid.
 */
#define COSC_EADDRESS -6

/**
 * @brief The pattern is invalid.
 */
#define COSC_EPATTERN -7

/**
 * @brief Did we find a bug?
 */
#define COSC_EBUG -127

/**
 * @brief The maximum size of any OSC data, that is
 * the largest 4 byte aligned, signed, 32-bit integer value.
 */
#define COSC_DATA_MAX 2147483644

#ifdef COSC_NOSTDLIB

#ifdef COSC_UINT8
typedef COSC_UINT8 uint8_t;
#else
typedef unsigned char uint8_t;
#endif

#ifdef COSC_INT32
typedef COSC_INT32 int32_t;
#else
typedef signed int int32_t;
#endif

#ifdef COSC_UINT32
typedef COSC_UINT32 uint32_t;
#else
typedef unsigned int uint32_t;
#endif

#ifdef COSC_INT64
typedef COSC_INT64 int64_t;
#else

#if defined(linux) && defined(__x86_64__)
typedef signed long int64_t;
#else
typedef signed long long int64_t;
#endif

#endif

#ifdef COSC_UINT64
typedef COSC_UINT64 uint64_t;
#else

#if defined(linux) && defined(__x86_64__)
typedef unsigned long uint64_t;
#else
typedef unsigned long long uint64_t;
#endif

#endif

#else

#include <stdint.h>

#endif

/**
 * @brief A value union.
 */
union cosc_value
{

    /**
     * @brief 'i' signed 32-bit integer.
     */
    int32_t i;

    /**
     * @brief 'f' 32-bit floating point.
     */
    float f;

    /**
     * @var S
     * @brief 'S' symbol (identical to 's').
     */

    /**
     * @brief 's' string (identical to 'S').
     */
    struct
    {

        /**
         * @brief The length payload, for strings this is excluding
         * the zero terminator.
         */
        int32_t length;

        /**
         * @brief Pointing to memory.
         */
        const char *ptr;

    } s, S;

    /**
     * @brief 's' string and 'S' symbol.
     */
    struct
    {

        /**
         * @brief The length payload, for strings this is excluding
         * the zero terminator.
         */
        int32_t length;

        /**
         * @brief Pointing to memory.
         */
        const void *ptr;

    } b;

    /**
     * @brief 'h' signed 64-bit integer.
     */
    int64_t h;

    /**
     * @brief 't' timetag, signed 64-bit integer.
     */
    uint64_t t;

    /**
     * @brief 'd' 64-bit floating point.
     */
    double d;

    /**
     * @brief 'c' single ASCII character.
     */
    uint8_t c;

    /**
     * @brief 'r' RGBA (unsigned 32-bit integer).
     */
    uint32_t r;

    /**
     * @brief 'm' MIDI message.
     */
    uint8_t m[4];

    /**
     * @brief 'T' true (1) and 'F' false (0).
     * @note This value is ignored when encoding.
     */
    int TF;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Get human readable string from error code.
 * @param error_code The error code.
 * @returns A pointer to a human readable error message.
 *
 * If the negative error code is unknown a pointer to the
 * static literal string "unknown error" is returned.
 *
 * If the error code is 0 or greater a pointer to an empty
 * static string is returned (no error).
 */
COSC_API const char *cosc_strerr
(
    int32_t error_code
);

/**
 * @brief Find the first invalid address character.
 * @param address The address.
 * @param address_size Read at most this many bytes from @p address.
 * @returns The index of the first invalid address character, if
 * no invalid character is found the returned index will reference
 * the zero terminator.
 *
 * Invalid characters are:
 *
 * - Space and below (ASCII 32 and below).
 * - # (ASCII 35)
 * - * (ASCII 42)
 * - , (ASCII 44)
 * - ? (ASCII 63)
 * - [ (ASCII 91)
 * - ] (ASCII 93)
 * - { (ASCII 123)
 * - } (ASCII 125)
 * - DEL (ASCII 127)
 * - Any negative value.
 */
COSC_API int32_t cosc_address_find_invalid
(
    const char *address,
    int32_t address_size
);

/**
 * @brief Find the first invalid typetag character.
 * @param typetag The typetag.
 * @param typetag_size Read at most this many bytes from @p typetag.
 * @returns The index of the first invalid address character, if
 * no invalid character is found the returned index will reference
 * the zero terminator.
 * @note The comma prefix may be omitted.
 *
 * Invalid characters are:
 *
 * - Space and below (ASCII 32 and below).
 * - DEL (ASCII 127)
 * - Any value negative value.
 */
COSC_API int32_t cosc_typetag_find_invalid
(
    const char *typetag,
    int32_t typetag_size
);

/**
 * @brief Match address with a pattern.
 * @param address The address to match with.
 * @param address_size Read at most this many bytes from @p address.
 * @param[out] address_offset If non-NULL the offset of the last matched
 * character in @p address is stored here.
 * @param pattern The pattern to match for.
 * @param pattern_size Read at most this many bytes from @p pattern.
 * @param[out] pattern_offset If non-NULL the offset of the last matched
 * character in @p pattern is stored here.
 * @returns 0 if the address matched the pattern or a negative error
 * code if the address could not be matched.
 * @note The address is not validated.
 *
 * Errors:
 * 
 * - @ref COSC_ERROR if @p address does not match the pattern.
 * - @ref COSC_EPATTERN if @p pattern is invalid.
 */
COSC_API int32_t cosc_address_match
(
    const char *address,
    int32_t address_size,
    int32_t *address_offset,
    const char *pattern,
    int32_t pattern_size,
    int32_t *pattern_offset
);

/**
 * @brief Convert a timetag to seconds.
 * @param timetag The timetag to convert.
 * @returns The time in seconds.
 */
COSC_API double cosc_timetag_to_seconds
(
    uint64_t timetag
);

/**
 * @brief Convert seconds to a timetag.
 * @param seconds The seconds.
 * @returns The timetag.
 * @note If the @p seconds will overflow the timetag the function
 * returns UINT64_MAX, if @p seconds is less than zero the function
 * returns 0.
 */
COSC_API uint64_t cosc_seconds_to_timetag
(
    double seconds
);

/**
 * @brief Convert a timetag to timeval.
 * @param timetag The timetag to convert.
 * @param[out] seconds If non-NULL store the seconds part here.
 * @param[out] nanoseconds If non-NULL store the nanoseconds part here.
 * @note This will reduce accuracy by aprox 1/5.
 */
COSC_API void cosc_timetag_to_timeval
(
    uint64_t timetag,
    int64_t *seconds,
    int64_t *nanoseconds
);

/**
 * @brief Convert timeval to a timetag.
 * @param seconds The seconds part.
 * @param nanoseconds The nanoseconds part.
 * @returns The timetag.
 * @note The timeval will be normalized, if the normalized time
 * is negative the function returns 0 and if the normalized time
 * overflows it's maximum the function returns UINT64_MAX.
 * @note Nanoseconds only have roughly 1/5 the resolution
 * of an NTP timestamp, results will not be rounded.
 */
COSC_API uint64_t cosc_timeval_to_timetag
(
    int64_t seconds,
    int64_t nanoseconds
);

/**
 * @brief Normalize a timeval.
 * @param seconds The seconds.
 * @param nanoseconds The nanoseconds.
 * @param[out] normalized_seconds If non-NULL store the normalized seconds
 * here.
 * @param[out] normalized_nanoseconds If non-NULL store the normalized
 * nanoseconds here.
 *
 * Nanoseconds overflowing into seconds will be converted to seconds.
 *
 * If the nanoseconds overflow is positive it is added to the
 * seconds capped at INT64_MAX.
 *
 * If the nanoseconds overflow is negative
 * it will be deducted from the seconds capped at 0.
 */
COSC_API void cosc_normalize_timeval
(
    int64_t seconds,
    int64_t nanoseconds,
    int64_t *normalized_seconds,
    int64_t *normalized_nanoseconds
);

/**
 * @brief Encode an unsigned, 8-bit integer.
 * @param value The value to encode.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 */
COSC_API int32_t cosc_encode_uint8
(
    uint8_t value,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode an unsigned, 8-bit integer.
 * @param[out] value If non-NULL returns a positive, non-zero value
 * the decoded value is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all of the required bytes.
 */
COSC_API int32_t cosc_decode_uint8
(
    uint8_t *value,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode a signed, 32-bit integer.
 * @param value The value to encode.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 */
COSC_API int32_t cosc_encode_int32
(
    int32_t value,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode a signed, 32-bit integer.
 * @param[out] value If non-NULL returns a positive, non-zero value
 * the decoded value is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all of the required bytes.
 */
COSC_API int32_t cosc_decode_int32
(
    int32_t *value,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode an unsigned, 32-bit integer.
 * @param value The value to encode.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 */
COSC_API int32_t cosc_encode_uint32
(
    uint32_t value,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode an unsigned, 32-bit integer.
 * @param[out] value If non-NULL returns a positive, non-zero value
 * the decoded value is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all of the required bytes.
 */
COSC_API int32_t cosc_decode_uint32
(
    uint32_t *value,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode a signed, 64-bit integer.
 * @param value The value to encode.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 */
COSC_API int32_t cosc_encode_int64
(
    int64_t value,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode a signed, 64-bit integer.
 * @param[out] value If non-NULL returns a positive, non-zero value
 * the decoded value is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all of the required bytes.
 */
COSC_API int32_t cosc_decode_int64
(
    int64_t *value,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode an unsigned, 64-bit integer.
 * @param value The value to encode.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 */
COSC_API int32_t cosc_encode_uint64
(
    uint64_t value,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode an unsigned, 64-bit integer.
 * @param[out] value If non-NULL returns a positive, non-zero value
 * the decoded value is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all of the required bytes.
 */
COSC_API int32_t cosc_decode_uint64
(
    uint64_t *value,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode a 32-bit floating point.
 * @param value The value to encode.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 */
COSC_API int32_t cosc_encode_float32
(
    float value,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode a 32-bit floating point.
 * @param[out] value If non-NULL returns a positive, non-zero value
 * the decoded value is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all of the required bytes.
 */
COSC_API int32_t cosc_decode_float32
(
    float *value,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode a 64-bit floating point.
 * @param value The value to encode.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 */
COSC_API int32_t cosc_encode_float64
(
    double value,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode a 64-bit floating point.
 * @param[out] value If non-NULL returns a positive, non-zero value
 * the decoded value is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all of the required bytes.
 */
COSC_API int32_t cosc_decode_float64
(
    double *value,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode a string.
 * @param string The string to encode.
 * @param string_size Read at most this many bytes from @p string.
 * @param[out] string_length If non-NULL and the function returns
 * a non-negative value the length of the encoded string,
 * excluding the zero terminator, is stored here.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 * - @ref COSC_EOVERFLOW if the encoded string size is >= @ref COSC_DATA_MAX.
 */
COSC_API int32_t cosc_encode_string
(
    const char *string,
    int32_t string_size,
    int32_t *string_length,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode a string.
 * @param[out] string If non-NULL and the function returns a non-negative
 * value a pointer to the string is stored here (will be the same as @p data).
 * @param[out] string_length If non-NULL and the function returns
 * a non-negative value the length of the decoded string,
 * excluding the zero terminator, is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error code
 * on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all the required bytes.
 * - @ref COSC_EOVERFLOW if the decoded string size is >= @ref COSC_DATA_MAX.
 */
COSC_API int32_t cosc_decode_string
(
    const char **string,
    int32_t *string_length,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode a typetag, almost the same as cosc_encode_string()
 * except that if a comma prefix is not found in @p typetag the
 * function will implicitly insert it first.
 * @param typetag The typetag to encode.
 * @param typetag_size Read at most this many bytes from @p typetag.
 * @param[out] typetag_length If non-NULL and the function returns
 * a non-negative value the length of the typetag, excluding the zero
 * terminator, is stored here.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 * - @ref COSC_EOVERFLOW if the encoded typetag size is >= @ref COSC_DATA_MAX.
 */
COSC_API int32_t cosc_encode_typetag
(
    const char *typetag,
    int32_t typetag_size,
    int32_t *typetag_length,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode a typetag, almost the same as cosc_decode_string()
 * except that it verifies the comma prefix.
 * @param[out] typetag If non-NULL and the function returns a non-negative
 * value a pointer to the typetag is stored here (will be the same as
 * @p data).
 * @param[out] typetag_length If non-NULL and the function returns
 * a non-negative value the length of the decoded typetag,
 * excluding the zero terminator, is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error code
 * on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all the required bytes.
 * - @ref COSC_EOVERFLOW if the decoded typetag size is >= @ref COSC_DATA_MAX.
 */
COSC_API int32_t cosc_decode_typetag
(
    const char **typetag,
    int32_t *typetag_length,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode a BLOB.
 * @param payload The payload of the BLOB, may be NULL only
 * if @p payload_size Is 0.
 * @param payload_size Copy this many bytes from @p payload. Note that
 * the largest size is @ref COSC_DATA_MAX - 4.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 */
COSC_API int32_t cosc_encode_blob
(
    const void *payload,
    int32_t payload_size,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode a BLOB.
 * function will implicitly add it.
 * @param[out] payload If non-NULL and the function returns a non-negative
 * a pointer to the payload data is stored here.
 * @param[out] payload_size If non-NULL and the function returns a positive
 * non-zero value the BLOB data size (in bytes) is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error code
 * on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all the required bytes.
 * - @ref COSC_EOVERFLOW if the decoded BLOB size is >= @ref COSC_DATA_MAX.
 */
COSC_API int32_t cosc_decode_blob
(
    const void **payload,
    int32_t *payload_size,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode a sequence of 3 MIDI bytes.
 * @param value The value to encode or NULL to zero the MIDI data.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 */
COSC_API int32_t cosc_encode_midi
(
    const uint8_t value[4],
    void *data,
    int32_t data_size
);

/**
 * @brief Decode a sequence of 3 MIDI bytes.
 * @param[out] value If non-NULL returns a positive, non-zero value
 * the decoded value is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all of the required bytes.
 */
COSC_API int32_t cosc_decode_midi
(
    uint8_t value[4],
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode a sequence of MIDI bytes, this does the same
 * as cosc_encode_midi(), but may be useful for bindings.
 * @param status The status byte.
 * @param byte1 The first data byte.
 * @param byte2 The second data byte.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 */
COSC_API int32_t cosc_encode_midi_args
(
    uint8_t status,
    uint8_t byte1,
    uint8_t byte2,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode a sequence of MIDI bytes, this does the same
 * as cosc_encode_midi(), but may be useful for bindings.
 * @param[out] status If non-NULL returns a positive, non-zero value
 * the decoded status byte is stored here.
 * @param[out] byte1 If non-NULL returns a positive, non-zero value
 * the decoded first byte is stored here.
 * @param[out] byte2 If non-NULL returns a positive, non-zero value
 * the decoded second byte is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all of the required bytes.
 */
COSC_API int32_t cosc_decode_midi_args
(
    uint8_t *status,
    uint8_t *byte1,
    uint8_t *byte2,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode a sequence of MIDI bytes, this does the same
 * as cosc_encode_midi(), but it will take a 14 bit value
 * and store it as two bytes. Useful for pitch bend and
 * song position messages, etc.
 * @param status The status byte.
 * @param value A 14-bit value.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns If @p data is non-NULL the function will return the
 * number of encoded bytes on success or a negative error code on failure.
 * If @p data is NULL the number of required bytes is returned.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is to small
 *   to fit all  the encoded bytes.
 */
COSC_API int32_t cosc_encode_midi14
(
    uint8_t status,
    uint32_t value,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode a sequence of MIDI bytes, this does the same
 * as cosc_encode_midi(), but it will take two bytes and
 * combine them into a 14-bit value. Useful for pitch bend and
 * song position messages, etc.
 * @param[out] status If non-NULL returns a positive, non-zero value
 * the decoded status byte is stored here.
 * @param[out] value If non-NULL returns a positive, non-zero value
 * the decoded 14-bit value is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data_size is not large enough to decode
 *   all of the required bytes.
 */
COSC_API int32_t cosc_decode_midi14
(
    uint8_t *status,
    uint32_t *value,
    const void *data,
    int32_t data_size
);

/**
 * @brief Check if the OSC data is a bundle.
 * @param data The data to check.
 * @param data_size Read at most this many bytes from @p data.
 * @returns True if the data is a valid bundle, false if not.
 */
COSC_API int32_t cosc_is_bundle
(
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode a bundle head.
 * @param timetag A timestamp for the entire bundle.
 * @param bundle_size The size of the bundle packets.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns The number of encoded bytes on success or a negative
 * error code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if the @p data_size is not large enough to contain
 *   all the bytes needed for decoding.
 * - @ref COSC_EOVERFLOW if @p bundle_size > @ref COSC_DATA_MAX.
 */
COSC_API int32_t cosc_encode_bundle_head
(
    uint64_t timetag,
    int32_t bundle_size,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode a bundle head.
 * @param[out] timetag If non-NULL and the function returns a
 * non-negative value the timestamp is stored here.
 * @param[out] bundle_size If non-NULL and the function returns 
 * non-negative value the bundle size is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative
 * error code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if the @p data_size is not large enough to contain
 *   all the bytes needed for decoding.
 * - @ref COSC_EOVERFLOW if the bundle_size > @ref COSC_DATA_MAX.
 * - @ref COSC_EADDRESS if the address is not "#bundle".
 */
COSC_API int32_t cosc_decode_bundle_head
(
    uint64_t *timetag,
    int32_t *bundle_size,
    const void *data,
    int32_t data_size
);

/**
 * @brief Encode values using value union.
 * @param typetag The typetag for the values, the comma prefix may
 * be omitted.
 * @param typetag_size Read at most this many bytes from @p typetag.
 * @param values A pointer to an array of @ref cosc_value .
 * @param[out] num_values If non-NULL and the function returns a positive
 * non-zero value the number encoded values is stored here.
 * @param[out] data If non-NULL the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data,
 * ignored if @p data is NULL.
 * @returns The number of encoded bytes on success or a negative
 * error code on failure.
 * @note The array of @p values must be at least of the same length
 * as the number of typetag types, even if they contain no payload data.
 * Members of @p values with types that have no payload are skipped.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if the @p data_size is not large enough to contain
 *   all the bytes needed for decoding.
 * - @ref COSC_ETYPETAG if the @p typetag contains invalid types.
 * - @ref COSC_EOVERFLOW if a size or offset is > @ref COSC_DATA_MAX.
 */
COSC_API int32_t cosc_encode_values
(
    const char *typetag,
    int32_t typetag_size,
    const union cosc_value *values,
    int32_t *num_values,
    void *data,
    int32_t data_size
);

/**
 * @brief Decode values using value union.
 * @param typetag The typetag for the values, the comma prefix may
 * be omitted.
 * @param typetag_size Read at most this many bytes from @p typetag.
 * @param[out] values A pointer to an array of @ref cosc_value
 * or pass NULL to not store any values.
 * @param max_values Only store at most this many values into @p values,
 * the rest are silently ignored. Ignored if @p values is NULL.
 * @param[out] num_values If non-NULL and the function returns a positive
 * non-zero value the number decoded values is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative
 * error code on failure.
 * @note The array of @p values must be at least of the same length
 * as the number of typetag types, even if they contain no payload data.
 * @note Types 'T' and 'F' do not have payload but the value member
 * will be written to.
 * All other members of @p values of types without payload will
 * be left uninitialized.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if the @p data_size is not large enough to contain
 *   all the bytes needed for decoding.
 * - @ref COSC_ETYPETAG if the @p typetag contains invalid types.
 * - @ref COSC_EOVERFLOW if a size or offset is > @ref COSC_DATA_MAX.
 */
COSC_API int32_t cosc_decode_values
(
    const char *typetag,
    int32_t typetag_size,
    union cosc_value *values,
    int32_t max_values,
    int32_t *num_values,
    const void *data,
    int32_t data_size
);

/**
 * @brief Convenience function to encode address and typetag in one go.
 * @param address The address or NULL for an empty address. The address
 * is not validated except for the fact that it must not be "#bundle".
 * @param address_size Read at most this many bytes from @p address.
 * @param typetag The typetag or NULL for an empty typetag, the comma prefix
 * may be omitted.
 * @param typetag_size Read at most this many bytes from @p typetag.
 * @param[out] data If non-NULL, @p data_size is greater than 0 and the
 * function returns a positive value the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data.
 * @returns The number of encoded bytes on success or a negative
 * error code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is
 *   not large enough to fit all the encoded bytes.
 * - @ref COSC_EOVERFLOW if a length will overflow.
 * - @ref COSC_EADDRESS if the address is "#bundle", the address
 *   will not be otherwise validated.
 */
COSC_API int32_t cosc_encode_message_head
(
    const char *address,
    int32_t address_size,
    const char *typetag,
    int32_t typetag_size,
    void *data,
    int32_t data_size
);

/**
 * @brief Convenience function to decode both address and typetag in one go.
 * @param[out] address_ptr If non-NULL and the function returns a positive
 * non-zero value a pointer to the address string is stored here.
 * @param[out] address_length If non-NULL and the function returns a positive
 * non-zero value the length of the address, excluding zero terminator,
 * is stored here.
 * @param[out] typetag_ptr If non-NULL and the function returns a positive
 * non-zero value a pointer to the the typetag string is stored here.
 * @param[out] typetag_length If non-NULL and the function returns a positive
 * non-zero value the length of the typetag, excluding zero terminator,
 * is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative
 * error code on failure.
 * @note This function does not validate the address or the typetag
 * except that address is not "#bundle" and the typetag must start
 * with a comma prefix.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if the @p data_size is not large enough to contain
 *   all the bytes needed for decoding.
 * - @ref COSC_EOVERFLOW if a string will length will overflow.
 * - @ref COSC_EADDRESS if the address is "#bundle".
 * - @ref COSC_ETYPETAG if the typetag does not start with a comma.
 */
COSC_API int32_t cosc_decode_message_head
(
    const char **address_ptr,
    int32_t *address_length,
    const char **typetag_ptr,
    int32_t *typetag_length,
    const void *data,
    int32_t data_size
);

/**
 * @brief Convenience function to encode an entire message in one go.
 * @param address The address or NULL for an empty address. The address
 * is not validated except for the fact that it must not be "#bundle".
 * @param address_size Read at most this many bytes from @p address.
 * @param typetag The typetag or NULL for an empty typetag, the comma
 * prefix may be omitted.
 * @param typetag_size Read at most this many bytes from @p typetag.
 * @param values A pointer to an array of @ref cosc_value .
 * @param[out] num_values If non-NULL and the function returns a positive
 * non-zero value the number encoded values is stored here.
 * @param[out] data If non-NULL, @p data_size is greater than 0 and the
 * function returns a positive value the encoded bytes are stored here.
 * @param data_size Store at most this many bytes in @p data.
 * @returns The number of encoded bytes on success or a negative
 * error code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is
 *   not large enough to fit all the encoded bytes.
 * - @ref COSC_EOVERFLOW if a length will overflow.
 * - @ref COSC_EADDRESS if the address is "#bundle", the address
 *   will not be otherwise validated.
 */
COSC_API int32_t cosc_encode_message
(
    const char *address,
    int32_t address_size,
    const char *typetag,
    int32_t typetag_size,
    const union cosc_value *values,
    int32_t *num_values,
    void *data,
    int32_t data_size
);

/**
 * @brief Convenience function to decode an entire message in one go.
 * @param[out] address_ptr If non-NULL and the function returns a positive
 * non-zero value a pointer to the address string is stored here.
 * @param[out] address_length If non-NULL and the function returns a positive
 * non-zero value the length of the address, excluding zero terminator,
 * is stored here.
 * @param[out] typetag_ptr If non-NULL and the function returns a positive
 * non-zero value a pointer to the the typetag string is stored here.
 * @param[out] typetag_length If non-NULL and the function returns a positive
 * non-zero value the length of the typetag, excluding zero terminator,
 * is stored here.
 * @param[out] values A pointer to an array of @ref cosc_value
 * or pass NULL to not store any values.
 * @param max_values Only store at most this many values into @p values,
 * the rest are silently ignored. Ignored if @p values is NULL.
 * @param[out] num_values If non-NULL and the function returns a positive
 * non-zero value the number decoded values is stored here.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative
 * error code on failure.
 * @note This function does not validate the address or typetag in @p data
 * except for checking that the address is not "#bundle" and that the
 * typetag starts with a comma.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if the @p data_size is not large enough to contain
 *   all the bytes needed for decoding.
 * - @ref COSC_EOVERFLOW if a string will length will overflow.
 * - @ref COSC_EADDRESS if the address is "#bundle".
 * - @ref COSC_ETYPETAG if the typetag does not start with a comma.
 */
COSC_API int32_t cosc_decode_message
(
    const char **address_ptr,
    int32_t *address_length,
    const char **typetag_ptr,
    int32_t *typetag_length,
    union cosc_value *values,
    int32_t max_values,
    int32_t *num_values,
    const void *data,
    int32_t data_size
);

/**
 * @brief Find total size, lengths of address and typetag and the offsets 
 * to the typetag and first value.
 * @param[out] address_length If non-NULL and the function returns
 * a non-negative value the address length is stored here.
 * @param[out] typetag_length If non-NULL and the function returns
 * a non-negative value the typetag length is stored here.
 * @param[out] typetag_offset If non-NULL and the function returns
 * a non-negative value the typetag offset is stored here.
 * @param[out] values_offset If non-NULL and the function returns
 * a non-negative value the offset of the first value is stored here.
 * @param[out] index_offset If non-NULL and the function returns
 * a non-negative value the offset of value specified by @p index
 * is stored here, will be -1 if @p index is invalid.
 * @param index If @p index_offset is non-NULL the function
 * will find the offset to this value.
 * @param data The data to decode.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative
 * error code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is
 *   not large enough to fit all the encoded bytes.
 * - @ref COSC_EOVERFLOW if the message is larger than @ref COSC_DATA_MAX.
 * - @ref COSC_ETYPETAG if the typetag contains invalid types.
 */
COSC_API int32_t cosc_find_message_offsets
(
    int32_t *address_length,
    int32_t *typetag_length,
    int32_t *typetag_offset,
    int32_t *values_offset,
    int32_t *index_offset,
    int32_t index,
    const void *data,
    int32_t data_size
);

/**
 * @brief Find the pointers and sizes of each individual component
 * of an OSC message.
 * @param[out] address If non-NULL and the function returns a non-negative
 * value a pointer to the address is stored here.
 * @param[out] typetag If non-NULL and the function returns a non-negative
 * value a pointer to the typetag is stored here.
 * @param index The index of the first value to get a pointer to. Ignored
 * if @p max_pointers is <= 0.
 * @param max_pointers The maximum number of pointers to get a pointer to.
 * @param[out] pointers If non-NULL and the function returns a non-negative
 * value the pointers to the pointers is stored in this array. Must be at least
 * the length specified in @p max_pointers. Note, these pointers should be
 * copied to aligned memory, do not type pun if you want to be C99 compatible.
 * @param[out] num_pointers If non-NULL and the function returns a non-negative
 * value the number of stored pointers and length is stored here.
 * @param data Decode this data to find all the pointers.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative
 * error code on failure.
 * @note The pointers stored in @p pointers may possibly memory misaligned
 * and should not be type punned if you want to remain compatible with C99.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is
 *   not large enough to fit all the encoded bytes.
 * - @ref COSC_EOVERFLOW if the message is larger than @ref COSC_DATA_MAX.
 * - @ref COSC_ETYPETAG if the typetag contains invalid types.
 * - @ref COSC_ERROR if @p max_pointers is > 0 and @p index is out of bounds.
 */
COSC_API int32_t cosc_find_message_pointers
(
    const char **address,
    const char **typetag,
    int32_t index,
    int32_t max_pointers,
    const void *pointers[],
    int32_t *num_pointers,
    const void *data,
    int32_t data_size
);

/**
 * @brief In place modification of the address string.
 * @param address The new address.
 * @param address_size Read at most this many bytes from @p address.
 * @param[out] address_length If non-NULL and the function returns a positive
 * non-zero value the length of the address, excluding the zero
 * terminator, is stored here.
 * @param[in,out] data Modify this OSC data.
 * @param data_size Read/store at most this many bytes from/in @p data.
 * @returns The new size of the OSC data on success or a negative error
 * code on failure.
 * @note This function does not validate the address in @p address or @p data
 * except for checking that the address is not "#bundle".
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if the @p data_size is not large enough to contain
 *   all the bytes needed for decoding/encoding.
 * - @ref COSC_EOVERFLOW if a string will length will overflow.
 * - @ref COSC_EADDRESS if the address in @p address or @p data is "#bundle".
 * - @ref COSC_ETYPETAG if the typetag in @p data has invalid types.
 */
COSC_API int32_t cosc_modify_set_address
(
    const char *address,
    int32_t address_size,
    int32_t *address_length,
    void *data,
    int32_t data_size
);

/**
 * @brief In place modification of the typetag string and values.
 * @param typetag The typetag for the values. The comma prefix
 * may be omitted.
 * @param typetag_size Read at most this many bytes from @p typetag.
 * @param values A pointer to an array of @ref cosc_value .
 * @param[out] num_values If non-NULL and the function returns a positive
 * non-zero value the number encoded values is stored here.
 * @param[in,out] data Modify this OSC data.
 * @param data_size Read/store at most this many bytes from/in @p data.
 * @returns The new size of the OSC data on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if the @p data_size is not large enough to contain
 *   all the bytes needed for decoding/encoding.
 * - @ref COSC_EOVERFLOW if a string will length will overflow.
 * - @ref COSC_EADDRESS if the address in @p address or @p data is "#bundle".
 * - @ref COSC_ETYPETAG if the typetag in @p data has invalid types.
 */
COSC_API int32_t cosc_modify_append_values
(
    const char *typetag,
    int32_t typetag_size,
    const union cosc_value *values,
    int32_t *num_values,
    void *data,
    int32_t data_size
);

/**
 * @brief In place modification of the typetag string and values.
 * @param index The index of where to insert the values.
 * @param typetag The typetag for the values. The comma prefix
 * may be omitted.
 * @param typetag_size Read at most this many bytes from @p typetag.
 * @param values A pointer to an array of @ref cosc_value .
 * @param[out] num_values If non-NULL and the function returns a positive
 * non-zero value the number encoded values is stored here.
 * @param[in,out] data Modify this OSC data.
 * @param data_size Read/store at most this many bytes from/in @p data.
 * @returns The new size of the OSC data on success or a negative error
 * code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if the @p data_size is not large enough to contain
 *   all the bytes needed for decoding/encoding.
 * - @ref COSC_EOVERFLOW if a string will length will overflow.
 * - @ref COSC_EADDRESS if the address in @p address or @p data is "#bundle".
 * - @ref COSC_ETYPETAG if the typetag in @p data has invalid types.
 * - @ref COSC_ERROR if the @p index is out of bounds.
 */
COSC_API int32_t cosc_modify_insert_values
(
    int32_t index,
    const char *typetag,
    int32_t typetag_size,
    const union cosc_value *values,
    int32_t *num_values,
    void *data,
    int32_t data_size
);

/**
 * @brief In place modification of the typetag string and values.
 * @param index The index of the first value to remove.
 * @param num The number of values to remove.
 * @param[in,out] data Modify this OSC data.
 * @param[out] num_values If non-NULL and the function returns a positive
 * non-zero value the number removed values is stored here.
 * @param data_size The new size of the data.
 * @returns The new size of the data on success. If @p index is out
 * of bounds the function returns @ref COSC_ERROR.
 */
COSC_API int32_t cosc_modify_remove_values
(
    int32_t index,
    int32_t num,
    int32_t *num_values,
    void *data,
    int32_t data_size
);

/**
 * @brief Swaps endian from big to host for all integer and
 * floating point values.
 * @param data Swap the endian for this OSC data.
 * @param data_size Read at most this many bytes from @p data.
 * @returns The number of decoded bytes on success or a negative
 * error code on failure.
 *
 * Errors:
 *
 * - @ref COSC_ESIZE if @p data is non-NULL and @p data_size is
 *   not large enough to fit all the encoded bytes.
 * - @ref COSC_EOVERFLOW if the message is larger than @ref COSC_DATA_MAX.
 * - @ref COSC_ETYPETAG if the typetag contains invalid types.
 */
COSC_API int32_t cosc_modify_swap_endian
(
    void *data,
    int32_t data_size
);

/**
 * @brief The size (in bytes) of the value union type.
 * @returns The size (in bytes) of the value union type.
 * @remark Helper for treating the value union type as an opaque type.
 */
COSC_API uint32_t cosc_value_csize
(
    void
);

#ifdef __cplusplus
}
#endif

#endif /* COSC_H */
