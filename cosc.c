/**
 * Copyright 2021 Peter Gebauer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "cosc.h"

#ifdef COSC_NOSTDLIB

#define COSC_NULL ((void *)0)
#define COSC_MEMSET cosc_memset_
#define COSC_MEMCPY cosc_memcpy_
#define COSC_MEMCMP cosc_memcmp_
#define COSC_MEMMOVE cosc_memmove_
#define COSC_STRNCMP cosc_strncmp_

#define UINT8_MAX 255
#define INT32_MIN (-((int32_t)-2147483647)-1)
#define INT32_MAX 2147483647
#define UINT32_MAX 4294967295
#define INT64_MIN (-((int64_t)9223372036854775807)-1)
#define INT64_MAX 9223372036854775807LL
#define UINT64_MAX 18446744073709551615ULL

static void *cosc_memset_(void *s, int c, uint32_t n)
{
    for (uint32_t i = 0; i < n; i++)
        ((uint8_t *)s)[i] = c;
    return s;
}

static void *cosc_memcpy_(void *dest, const void *src, uint32_t n)
{
    for (uint32_t i = 0; i < n; i++)
        ((uint8_t *)dest)[i] = ((const uint8_t *)src)[i];
    return dest;
}

static int32_t cosc_memcmp_(const void *v1, const void *v2, uint32_t n)
{
    for (uint32_t i = 0; i < n; i++)
    {
        if (((const uint8_t *)v1)[i] < ((const uint8_t *)v2)[i])
            return -1;
        else if (((const uint8_t *)v1)[i] > ((const uint8_t *)v2)[i])
            return 1;
    }
    return 0;
}

static void *cosc_memmove_(void *dest, const void *src, uint32_t n)
{
    uint8_t *pd;
    const uint8_t *ps;
    if (dest != src)
    {
        pd = dest;
        ps = src;
        if (pd > ps && pd < ps + n)
        {
            pd += n;
            ps += n;
            while (n-- > 0)
                *--pd = *--ps;
        }
        else
        {
            while (n-- > 0)
                *pd++ = *ps++;
        }
    }
    return dest;
}

static int cosc_strncmp_(const char *s1, const char *s2, uint32_t n)
{
    uint32_t l = 0;
    while (l < n && s1[0] != 0)
    {
        if (s1[l] < s2[l])
            return -1;
        else if (s1[l] > s2[l])
            return 1;
        l++;
    }
    return 0;
}

#else

#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

#define COSC_NULL NULL
#define COSC_MEMSET memset
#define COSC_MEMCPY memcpy
#define COSC_MEMCMP memcmp
#define COSC_MEMMOVE memmove
#define COSC_STRNCMP strncmp

#endif

#define COSC_ALIGNED_STRING_SIZE_(stringlen) ((stringlen) + 4 - ((stringlen) % 4));
#define COSC_ALIGNED_BLOB_SIZE_(blobsize) ((((blobsize) + 3) / 4) * 4);
#define COSC_IS_BUNDLE_(data_, data_size_) ((data_size_) >= 8 && COSC_MEMCMP(data_, "#bundle", 8) == 0)

#define COSC_U32BE_STORE_(value, buffer) \
    ((uint8_t *)(buffer))[0] = ((value) & 0xff000000) >> 24; \
    ((uint8_t *)(buffer))[1] = ((value) & 0xff0000) >> 16; \
    ((uint8_t *)(buffer))[2] = ((value) & 0xff00) >> 8; \
    ((uint8_t *)(buffer))[3] = ((value) & 0xff)

#define COSC_U32BE_LOAD_(value, buffer) \
    (value) = ((const uint8_t *)(buffer))[0]; (value) <<= 8; \
    (value) |= ((const uint8_t *)(buffer))[1]; (value) <<= 8; \
    (value) |= ((const uint8_t *)(buffer))[2]; (value) <<= 8; \
    (value) |= ((const uint8_t *)(buffer))[3]

#define COSC_ENCODE_U32BE_(value, buffer) \
    do { \
        COSC_U32BE_STORE_(value, buffer); \
    } while (0)

#define COSC_DECODE_U32BE_(value, buffer) \
    do { \
        COSC_U32BE_LOAD_(value, buffer); \
    } while (0)

#define COSC_ENCODE_S32BE_(value, buffer) \
    do { \
        uint32_t __tmpuint__; \
        COSC_MEMCPY(&__tmpuint__, &value, 4); \
        COSC_U32BE_STORE_(__tmpuint__, buffer); \
    } while (0)

#define COSC_DECODE_S32BE_(value, buffer) \
    do { \
        uint32_t __tmpuint__; \
        COSC_U32BE_LOAD_(__tmpuint__, buffer); \
        COSC_MEMCPY(&value, &__tmpuint__, 4); \
    } while (0)

#define COSC_ENCODE_F32BE_(value, buffer) \
    do { \
        uint32_t __tmpuint__; \
        COSC_MEMCPY(&__tmpuint__, &value, 4); \
        COSC_U32BE_STORE_(__tmpuint__, buffer); \
    } while (0)

#define COSC_DECODE_F32BE_(value, buffer) \
    do { \
        uint32_t __tmpuint__; \
        COSC_U32BE_LOAD_(__tmpuint__, buffer); \
        COSC_MEMCPY(&value, &__tmpuint__, 4); \
    } while (0)

#define COSC_U64BE_STORE_(value, buffer) \
    ((uint8_t *)(buffer))[0] = ((value) & 0xff00000000000000) >> 56; \
    ((uint8_t *)(buffer))[1] = ((value) & 0xff000000000000) >> 48; \
    ((uint8_t *)(buffer))[2] = ((value) & 0xff0000000000) >> 40; \
    ((uint8_t *)(buffer))[3] = ((value) & 0xff00000000) >> 32; \
    ((uint8_t *)(buffer))[4] = ((value) & 0xff000000) >> 24; \
    ((uint8_t *)(buffer))[5] = ((value) & 0xff0000) >> 16; \
    ((uint8_t *)(buffer))[6] = ((value) & 0xff00) >> 8; \
    ((uint8_t *)(buffer))[7] = ((value) & 0xff)

#define COSC_U64BE_LOAD_(value, buffer) \
    (value) = ((const uint8_t *)(buffer))[0]; (value) <<= 8; \
    (value) |= ((const uint8_t *)(buffer))[1]; (value) <<= 8; \
    (value) |= ((const uint8_t *)(buffer))[2]; (value) <<= 8; \
    (value) |= ((const uint8_t *)(buffer))[3]; (value) <<= 8; \
    (value) |= ((const uint8_t *)(buffer))[4]; (value) <<= 8; \
    (value) |= ((const uint8_t *)(buffer))[5]; (value) <<= 8; \
    (value) |= ((const uint8_t *)(buffer))[6]; (value) <<= 8; \
    (value) |= ((const uint8_t *)(buffer))[7]

#define COSC_ENCODE_U64BE_(value, buffer) \
    do { \
        COSC_U64BE_STORE_(value, buffer); \
    } while (0)

#define COSC_DECODE_U64BE_(value, buffer) \
    do { \
        COSC_U64BE_LOAD_(value, buffer); \
    } while (0)

#define COSC_ENCODE_S64BE_(value, buffer) \
    do { \
        uint64_t __tmpuint__; \
        COSC_MEMCPY(&__tmpuint__, &value, 8); \
        COSC_U64BE_STORE_(__tmpuint__, buffer); \
    } while (0)

#define COSC_DECODE_S64BE_(value, buffer) \
    do { \
        uint64_t __tmpuint__; \
        COSC_U64BE_LOAD_(__tmpuint__, buffer); \
        COSC_MEMCPY(&value, &__tmpuint__, 8); \
    } while (0)

#define COSC_ENCODE_F64BE_(value, buffer) \
    do { \
        uint64_t __tmpuint__; \
        COSC_MEMCPY(&__tmpuint__, &value, 8); \
        COSC_U64BE_STORE_(__tmpuint__, buffer); \
    } while (0)

#define COSC_DECODE_F64BE_(value, buffer) \
    do { \
        uint64_t __tmpuint__; \
        COSC_U64BE_LOAD_(__tmpuint__, buffer); \
        COSC_MEMCPY(&value, &__tmpuint__, 8); \
    } while (0)

inline static int32_t cosc_encode_string_
(
    const char *string,
    int32_t string_size,
    int32_t *string_length,
    void *data,
    int32_t data_size
)
{
    int32_t len = 0;
    while (len < string_size && string[len] != 0)
    {
        if (len >= COSC_DATA_MAX) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
        len++;
    }
    int32_t aligned_size = COSC_ALIGNED_STRING_SIZE_(len);
    if (data != COSC_NULL)
    {
        if (data_size < aligned_size)
            return COSC_ESIZE;
        if (len > 0)
            COSC_MEMCPY(data, string, len);
        COSC_MEMSET((uint8_t *)data + len, 0, aligned_size - len);
    }
    if (string_length != COSC_NULL)
        *string_length = len;
    return aligned_size;
}

inline static int32_t cosc_decode_string_
(
    const char **string,
    int32_t *string_length,
    const void *data,
    int32_t data_size
)
{
    int32_t aligned_size, len;
    if (data_size < 4)
        return COSC_ESIZE;
    len = 0;
    while (len < data_size && ((const uint8_t *)data)[len] != 0)
    {
        if (len >= COSC_DATA_MAX) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
        len++;
    }
    aligned_size = COSC_ALIGNED_STRING_SIZE_(len);
    if (data_size < aligned_size)
        return COSC_ESIZE;
    if (string_length!= COSC_NULL)
        *string_length = len;
    if (string != COSC_NULL)
        *string = data;
    return aligned_size;
}

inline static int32_t cosc_encode_typetag_
(
    const char *typetag,
    int32_t typetag_size,
    int32_t *typetag_length,
    void *data,
    int32_t data_size
)
{
    int32_t len = 0;
    while (len < typetag_size && typetag[len] != 0)
    {
        if (len >= COSC_DATA_MAX) return COSC_EOVERFLOW;  // GCOVR_EXCL_LINE
        len++;
    }
    int32_t prefixed_len = len;
    if (len == 0 || typetag[0] != ',')
    {
        prefixed_len++;
        if (prefixed_len >= COSC_DATA_MAX) return COSC_EOVERFLOW;  // GCOVR_EXCL_LINE
    }
    int32_t aligned_size = COSC_ALIGNED_STRING_SIZE_(prefixed_len);
    if (data != COSC_NULL)
    {
        if (data_size < aligned_size)
            return COSC_ESIZE;
        if (prefixed_len > len)
        {
            ((uint8_t *)data)[0] = ',';
            if (len > 0)
                COSC_MEMCPY((uint8_t *)data + 1, typetag, len);
            len = prefixed_len;
        }
        else
            COSC_MEMCPY(data, typetag, len);
        COSC_MEMSET((uint8_t *)data + prefixed_len, 0, aligned_size - prefixed_len);
    }
    if (typetag_length!= COSC_NULL)
        *typetag_length = len;
    return aligned_size;
}

inline static int32_t cosc_decode_typetag_
(
    const char **typetag,
    int32_t *typetag_length,
    const void *data,
    int32_t data_size
)
{
    int32_t aligned_size, len;
    if (data_size < 4)
        return COSC_ESIZE;
    len = 0;
    while (len < data_size
           && ((const uint8_t *)data)[len] != 0)
        len++;
    if (typetag_length != COSC_NULL)
        *typetag_length = len;
    aligned_size = COSC_ALIGNED_STRING_SIZE_(len);
    if (data_size < aligned_size)
        return COSC_ESIZE;
    if (typetag_length!= COSC_NULL)
        *typetag_length = len;
    if (typetag != COSC_NULL)
        *typetag = data;
    return aligned_size;
}


inline static int32_t cosc_encode_blob_
(
    const void *payload,
    int32_t payload_size,
    void *data,
    int32_t data_size
)
{
    int32_t aligned_size, aligned_psize;
    if (payload_size > COSC_DATA_MAX - 4)
        return COSC_EOVERFLOW;
    if (payload_size < 0)
        payload_size = 0;
    aligned_psize = payload_size;
    aligned_size = COSC_ALIGNED_BLOB_SIZE_(aligned_psize);
    aligned_size += 4;
    if (data != COSC_NULL)
    {
        if (data_size < aligned_size)
            return COSC_ESIZE;
        COSC_ENCODE_S32BE_(payload_size, data);
        data = (uint8_t *)data + 4;
        if (payload_size > 0)
        {
            if (payload == COSC_NULL)
                COSC_MEMSET(data, 0, payload_size);
            else
                COSC_MEMCPY(data, payload, payload_size);
            if (aligned_psize > payload_size)
                COSC_MEMSET((uint8_t *)data + payload_size, 0, aligned_psize - payload_size);
        }
    }
    return aligned_size;
}

inline static int32_t cosc_decode_blob_
(
    const void **payload,
    int32_t *payload_size,
    const void *data,
    int32_t data_size
)
{
    uint32_t psize;
    int32_t aligned_size;
    if (data_size < 4)
        return COSC_ESIZE;
    COSC_DECODE_U32BE_(psize, data);
    if (psize > COSC_DATA_MAX - 4)
        return COSC_EOVERFLOW;
    aligned_size = psize + 4;
    aligned_size = COSC_ALIGNED_BLOB_SIZE_(aligned_size);
    if (data_size < aligned_size)
        return COSC_ESIZE;
    if (payload_size != COSC_NULL)
        *payload_size = psize;
    if (payload != COSC_NULL)
        *payload = ((const uint8_t *)data) + 4;
    return aligned_size;
}

inline static int32_t cosc_find_message_offsets_
(
    int32_t *address_length,
    int32_t *typetag_length,
    int32_t *typetag_offset,
    int32_t *values_offset,
    int32_t *index_offset,
    int32_t index,
    const void *data,
    int32_t data_size
)
{
    int32_t offset, ret;
    int32_t tmp_address_length, tmp_typetag_length, tmp_typetag_offset, tmp_values_offset, tmp_index_offset;
    int32_t i;
    const char *typetag;
    if (data_size < 4)
        return COSC_ESIZE;
    offset = 0;
    ret = cosc_decode_string_(COSC_NULL, &tmp_address_length, data, data_size);
    if (ret < 0)
        return ret;
    offset += ret;
    data_size -= ret;
    tmp_typetag_offset = offset;
    ret = cosc_decode_typetag_(&typetag, &tmp_typetag_length, (const uint8_t *)data + offset, data_size);
    if (ret < 0)
        return ret;
    offset += ret;
    data_size -= ret;
    tmp_values_offset = offset;
    if (tmp_typetag_length > 0 && typetag[0] == ',')
        typetag++;

    tmp_index_offset = -1;
    for (i = 0; typetag[i] != 0; i++)
    {
        if (i == index)
            tmp_index_offset = offset;
        switch (typetag[i])
        {
        case 'i':
        case 'f':
        case 'c':
        case 'r':
        case 'm':
            ret = data_size >= 4 ? 4 : COSC_ESIZE;
            break;
        case 's':
        case 'S':
            ret = cosc_decode_string_(COSC_NULL, COSC_NULL, (const uint8_t *)data + offset, data_size);
            break;
        case 'b':
            ret = cosc_decode_blob_(COSC_NULL, COSC_NULL, (const uint8_t *)data + offset, data_size);
            break;
        case 'h':
        case 't':
        case 'd':
            ret = data_size >= 8 ? 8 : COSC_ESIZE;
            break;
        case 'T':
        case 'F':
        case 'N':
        case 'I':
            ret = 0;
            break;
        default:
            ret = COSC_ETYPETAG;
            break;
        }
        if (ret < 0)
            return ret;
        if (offset > COSC_DATA_MAX - ret) return COSC_EOVERFLOW;  // GCOVR_EXCL_LINE
        offset += ret;
        data_size -= ret;
    }

    if (address_length != COSC_NULL)
        *address_length = tmp_address_length;
    if (typetag_length != COSC_NULL)
        *typetag_length = tmp_typetag_length;
    if (typetag_offset != COSC_NULL)
        *typetag_offset = tmp_typetag_offset;
    if (values_offset != COSC_NULL)
        *values_offset = tmp_values_offset;
    if (index_offset != COSC_NULL)
        *index_offset = tmp_index_offset;
    return offset;
}

static inline int32_t cosc_find_message_pointers_
(
    const char **address,
    const char **typetag,
    int32_t index,
    int32_t max_pointers,
    const void *pointers[],
    int32_t *num_pointers,
    const void *data,
    int32_t data_size
)
{
    const char *addr, *ttag, *origttag;
    int32_t addrlen, ttaglen;
    int32_t offset = 0;
    int32_t ret = cosc_decode_string_(&addr, &addrlen, data, data_size);
    if (ret < 0)
        return ret;
    offset += ret;
    ret = cosc_decode_typetag_(&origttag, &ttaglen, (const uint8_t *)data + offset, data_size - offset);
    if (ret < 0)
        return ret;
    offset += ret;
    if (max_pointers > 0 && (index < 0 || index >= ttaglen))
        return COSC_ERROR;

    ttag = origttag;
    if (ttag[0] == ',')
    {
        ttag++;
        ttaglen--;
    }

    int32_t value_count = 0;
    for (int32_t i = 0; i < ttaglen; i++)
    {
        switch (ttag[i])
        {
        case 'i':
        case 'f':
        case 'c':
        case 'r':
        case 'm':
            ret = data_size - offset >= 4 ? 4 : COSC_ESIZE;
            break;
        case 's':
        case 'S':
            ret = cosc_decode_string_(COSC_NULL, COSC_NULL, (const uint8_t *)data + offset, data_size - offset);
            break;
        case 'b':
            ret = cosc_decode_blob_(COSC_NULL, COSC_NULL, (const uint8_t *)data + offset, data_size - offset);
            break;
        case 'h':
        case 't':
        case 'd':
            ret = data_size - offset >= 8 ? 8 : COSC_ESIZE;
            break;
        case 'T':
        case 'F':
        case 'N':
        case 'I':
            ret = 0;
            break;
        default:
            ret = COSC_ETYPETAG;
            break;
        }

        if (ret < 0)
            return ret;
        if (i >= index)
        {
            if (value_count < max_pointers && pointers != COSC_NULL)
            {
                if (ret > 0)
                    pointers[value_count] = (const uint8_t *)data + offset;
                else
                    pointers[value_count] = COSC_NULL;
            }
            value_count++;
        }
        offset += ret;
    }
    if (address != COSC_NULL)
        *address = addr;
    if (typetag != COSC_NULL)
        *typetag = origttag;
    if (num_pointers != COSC_NULL)
        *num_pointers = value_count;
    return offset;
}

inline static void cosc_normalize_timeval_
(
    int64_t *seconds,
    int64_t *nanoseconds
)
{
    int64_t nano_overflow = *nanoseconds / 1000000000LL;
    if (nano_overflow > 0)
    {
        if (*seconds > INT64_MAX - nano_overflow)
        {
            *seconds = INT64_MAX;
            *nanoseconds = 999999999LL;
        }
        else
        {
            *seconds += nano_overflow;
            *nanoseconds %= 1000000000LL;
        }
    }
    else if (nano_overflow < 0)
    {
        if (*seconds <= 0)
        {
            *seconds = 0;
            *nanoseconds = 0;
        }
        else
        {
            *seconds += nano_overflow;
            *nanoseconds %= 1000000000LL;
            *nanoseconds += 1000000000LL;
            *seconds -= 1;
        }
    }
    else if (*nanoseconds < 0)
    {
        if (*seconds < 1)
        {
            *seconds = 0;
            *nanoseconds = 0;
        }
        else
        {
            *seconds -= 1;
            *nanoseconds += 1000000000;
        }
    }
}

const char *cosc_strerr
(
    int32_t error_code
)
{
    if (error_code >= 0)
        return "";
    switch (error_code)
    {
    case COSC_ERROR:
        return "generic error";
    case COSC_ENOMEM:
        return "out of memory";
    case COSC_EOVERFLOW:
        return "integer overflow";
    case COSC_ESIZE:
        return "data size too small";
    case COSC_ETYPETAG:
        return "invalid typetag";
    case COSC_EADDRESS:
        return "invalid address";
    case COSC_EPATTERN:
        return "invalid pattern";
    }
    return "unknown error";
}

int32_t cosc_address_find_invalid
(
    const char *address,
    int32_t address_size
)
{
    static const char invalid_address_chars[] = "#*,?[]{}";
    int32_t i, j;
    for (i = 0; i < address_size && address[i] != 0; i++)
    {
        if (address[i] <= 32 || address[i] >= 127)
            return i;
        for (j = 0; invalid_address_chars[j] != 0; j++)
            if (address[i] == invalid_address_chars[j])
                return i;
    }
    return COSC_ERROR;
}

int32_t cosc_typetag_find_invalid
(
    const char *typetag,
    int32_t typetag_size
)
{
    static const char valid_typetag_chars[] = "ifsbhtdScrmTFNI";
    int32_t i = 0, j;
    if (typetag_size > 0 && typetag[0] == ',')
        i++;
    for (; i < typetag_size && typetag[i] != 0; i++)
    {
        if (typetag[i] <= 32 || typetag[i] >= 127)
            return i;
        for (j = 0; valid_typetag_chars[j] != 0; j++)
            if (typetag[i] == valid_typetag_chars[j])
                break;
        if (valid_typetag_chars[j] == 0)
            return i;
    }
    return COSC_ERROR;
}

int32_t cosc_address_match
(
    const char *address,
    int32_t address_size,
    int32_t *address_offset,
    const char *pattern,
    int32_t pattern_size,
    int32_t *pattern_offset
)
{
    int32_t pattern_ofs, address_ofs, tmp, err;
    if (pattern == COSC_NULL || pattern_size == 0)
        pattern = "";
    if (address == COSC_NULL || address_size == 0)
        address = "";
    else if (pattern[0] == 0 && address[0] != 0)
    {
        if (pattern_offset != COSC_NULL)
            *pattern_offset = 0;
        return COSC_ERROR;
    }
    pattern_ofs = 0;
    address_ofs = 0;
    err = 0;

    while (pattern_ofs < pattern_size
           && pattern[pattern_ofs] != 0)
    {

        switch (pattern[pattern_ofs])
        {
        case '?':
            if (address_ofs >= address_size || address[address_ofs] == 0)
            {
                err = COSC_ERROR;
                goto done;
            }
            address_ofs++;
            pattern_ofs++;
            break;

        case '*':
            while (pattern[pattern_ofs] == '*')
                pattern_ofs++;
            while (address_ofs < address_size && address[address_ofs] != 0 && address[address_ofs] != pattern[pattern_ofs])
                address_ofs++;
            break;

        case '[':
            pattern_ofs++;
            if (pattern_ofs >= pattern_size || pattern[pattern_ofs] == 0)
            {
                err = COSC_EPATTERN;
                goto done;
            }
            if (pattern[pattern_ofs] == ']')
            {
                pattern_ofs++;
                break;
            }
            if (pattern_size - pattern_ofs >= 2 && pattern[pattern_ofs] == '!' && pattern[pattern_ofs + 1] == ']')
            {
                pattern_ofs += 2;
                break;
            }

            if (pattern_size - pattern_ofs >= 4
                && pattern[pattern_ofs] != 0
                && pattern[pattern_ofs + 1] == '-'
                && pattern[pattern_ofs + 2] != 0
                && pattern[pattern_ofs + 3] == ']'
            )
            {
                char min = pattern[pattern_ofs] < pattern[pattern_ofs + 2] ? pattern[pattern_ofs] : pattern[pattern_ofs + 2];
                char max = pattern[pattern_ofs] > pattern[pattern_ofs + 2] ? pattern[pattern_ofs] : pattern[pattern_ofs + 2];
                if (address[address_ofs] < min || address[address_ofs] > max)
                {
                    err = COSC_ERROR;
                    goto done;
                }
                address_ofs++;
                pattern_ofs += 4;
            }
            else
            {

                int fail = 0;
                if (pattern[pattern_ofs] == '!')
                {
                    pattern_ofs++;
                    while (pattern_ofs < pattern_size && pattern[pattern_ofs] != 0 && pattern[pattern_ofs] != ']')
                    {
                        if (pattern[pattern_ofs] == address[address_ofs])
                        {
                            fail = 1;
                            break;
                        }
                        pattern_ofs++;
                    }
                }
                else
                {
                    fail = 1;
                    while (pattern_ofs < pattern_size && pattern[pattern_ofs] != 0 && pattern[pattern_ofs] != ']')
                    {
                        if (pattern[pattern_ofs] == address[address_ofs])
                        {
                            fail = 0;
                            break;
                        }
                        pattern_ofs++;
                    }
                }
                address_ofs++;
                while (pattern_ofs < pattern_size
                       && pattern[pattern_ofs] != 0
                       && pattern[pattern_ofs] != ']')
                    pattern_ofs++;
                if (pattern[pattern_ofs] != ']')
                {
                    err = COSC_EPATTERN;
                    goto done;
                }
                if (pattern_ofs < pattern_size)
                    pattern_ofs++;
                if (fail)
                {
                    err = COSC_ERROR;
                    goto done;
                }
            }
            break;

        case '{':
            pattern_ofs++;
            if (pattern_ofs >= pattern_size || pattern[pattern_ofs] == 0)
            {
                err = COSC_EPATTERN;
                goto done;
            }
            if (pattern[pattern_ofs] != '}')
            {
                // while (pattern_ofs < pattern_size
                //        && pattern[pattern_ofs] != 0
                //        && pattern[pattern_ofs] != '}')
                while (1)
                {
                    tmp = address_ofs;
                    while (pattern_ofs < pattern_size
                           && pattern[pattern_ofs] != 0
                           && pattern[pattern_ofs] != ','
                           && pattern[pattern_ofs] != '}'
                           && tmp < address_size
                           && address[tmp] == pattern[pattern_ofs])
                    {
                        pattern_ofs++;
                        tmp++;
                    }
                    if (pattern_ofs >= pattern_size || pattern[pattern_ofs] == 0)
                    {
                        err = COSC_EPATTERN;
                        goto done;
                    }
                    if (pattern[pattern_ofs] == ',' || pattern[pattern_ofs] == '}')
                    {
                        address_ofs = tmp;
                        while (pattern_ofs < pattern_size
                               && pattern[pattern_ofs] != 0
                               && pattern[pattern_ofs] != '}')
                            pattern_ofs++;
                        break;
                    }
                    else
                    {
                        while (pattern_ofs < pattern_size
                               && pattern[pattern_ofs] != 0
                               && pattern[pattern_ofs] != ','
                               && pattern[pattern_ofs] != '}')
                            pattern_ofs++;
                        if (pattern_ofs >= pattern_size || pattern[pattern_ofs] == 0)
                        {
                            if (pattern_offset != COSC_NULL)
                                *pattern_offset = pattern_ofs;
                            return COSC_EPATTERN;
                        }
                        if (pattern[pattern_ofs] == '}')
                        {
                            err = COSC_ERROR;
                            goto done;
                        }
                        pattern_ofs++;
                    }
                }
                if (pattern_ofs >= pattern_size || pattern[pattern_ofs] != '}')
                {
                    err = COSC_EPATTERN;
                    goto done;
                }
                pattern_ofs++;
            }
            else
                pattern_ofs++;
            break;

        default:
            if (address_ofs >= address_size
                || address[address_ofs] == 0
                || address[address_ofs] != pattern[pattern_ofs])
            {
                err = COSC_ERROR;
                goto done;
            }
            address_ofs++;
            pattern_ofs++;
            break;
        }
    }

done:
    if (address_offset != COSC_NULL)
        *address_offset = address_ofs;
    if (pattern_offset != COSC_NULL)
        *pattern_offset = pattern_ofs;
    if (err < 0)
        return err;
    if (address[address_ofs] != 0)
        return COSC_ERROR;
    return address_ofs;
}

double cosc_timetag_to_seconds
(
    uint64_t timetag
)
{
    return (timetag >> 32) + (timetag & UINT32_MAX) / 4294967296.0;
}

uint64_t cosc_seconds_to_timetag
(
    double seconds
)
{
    if (seconds > UINT32_MAX)
        return UINT64_MAX;
    if (seconds < 0)
        return 0;
    int64_t timetag = seconds;
    seconds -= timetag;
    timetag <<= 32;
    timetag += seconds * 4294967296;
    return timetag;
}

void cosc_timetag_to_timeval
(
    uint64_t timetag,
    int64_t *seconds,
    int64_t *nanoseconds
)
{
    if (seconds != COSC_NULL)
        *seconds = timetag >> 32;
    if (nanoseconds != COSC_NULL)
        *nanoseconds = (timetag & 0xffffffff) * 1000000000  / 4294967296;
}

uint64_t
cosc_timeval_to_timetag
(
    int64_t seconds,
    int64_t nanoseconds
)
{
    cosc_normalize_timeval_(&seconds, &nanoseconds);
    if (seconds > UINT32_MAX)
        return UINT64_MAX;
    uint64_t t = (seconds << 32) | (nanoseconds * 4294967296 / 1000000000);
    if (nanoseconds > 0)
        t++;
    return t;
}

void cosc_normalize_timeval
(
    int64_t seconds,
    int64_t nanoseconds,
    int64_t *normalized_seconds,
    int64_t *normalized_nanoseconds
)
{
    cosc_normalize_timeval_(&seconds, &nanoseconds);
    if (normalized_seconds != COSC_NULL)
        *normalized_seconds = seconds;
    if (normalized_nanoseconds != COSC_NULL)
        *normalized_nanoseconds = nanoseconds;
}

int32_t cosc_encode_uint8
(
    uint8_t value,
    void *data,
    int32_t data_size
)
{
    if (data != COSC_NULL)
    {
        if (data_size < 4)
            return COSC_ESIZE;
        ((uint8_t *)data)[0] = value;
        ((uint8_t *)data)[1] = 0;
        ((uint8_t *)data)[2] = 0;
        ((uint8_t *)data)[3] = 0;
    }
    return 4;
}

int32_t cosc_decode_uint8
(
    uint8_t *value,
    const void *data,
    int32_t data_size
)
{
    if (data_size < 4)
        return COSC_ESIZE;
    if (value != COSC_NULL)
        *value = ((const uint8_t *)data)[0];
    return 4;
}

int32_t cosc_encode_int32
(
    int32_t value,
    void *data,
    int32_t data_size
)
{
    if (data != COSC_NULL)
    {
        if (data_size < 4)
            return COSC_ESIZE;
        COSC_ENCODE_S32BE_(value, data);
    }
    return 4;
}

int32_t cosc_decode_int32
(
    int32_t *value,
    const void *data,
    int32_t data_size
)
{
    if (data_size < 4)
        return COSC_ESIZE;
    if (value != COSC_NULL)
    {
        COSC_DECODE_S32BE_(*value, data);
    }
    return 4;
}


int32_t cosc_encode_uint32
(
    uint32_t value,
    void *data,
    int32_t data_size
)
{
   if (data != COSC_NULL)
   {
       if (data_size < 4)
           return COSC_ESIZE;
       COSC_ENCODE_U32BE_(value, data);
   }
   return 4;
}

int32_t cosc_decode_uint32
(
    uint32_t *value,
    const void *data,
    int32_t data_size
)
{
    if (data_size < 4)
        return COSC_ESIZE;
    if (value != COSC_NULL)
    {
        COSC_DECODE_U32BE_(*value, data);
    }
    return 4;
}

int32_t cosc_encode_int64
(
    int64_t value,
    void *data,
    int32_t data_size
)
{
    if (data != COSC_NULL)
    {
        if (data_size < 8)
            return COSC_ESIZE;
        COSC_ENCODE_S64BE_(value, data);
    }
    return 8;
}

int32_t cosc_decode_int64
(
    int64_t *value,
    const void *data,
    int32_t data_size
)
{
    if (data_size < 8)
        return COSC_ESIZE;
    if (value != COSC_NULL)
    {
        COSC_DECODE_S64BE_(*value, data);
    }
    return 8;
}

int32_t cosc_encode_uint64
(
    uint64_t value,
    void *data,
    int32_t data_size
)
{
   if (data != COSC_NULL)
   {
       if (data_size < 8)
           return COSC_ESIZE;
       COSC_ENCODE_U64BE_(value, data);
   }
   return 8;
}

int32_t cosc_decode_uint64
(
    uint64_t *value,
    const void *data,
    int32_t data_size
)
{
    if (data_size < 8)
        return COSC_ESIZE;
    if (value != COSC_NULL)
    {
        COSC_DECODE_U64BE_(*value, data);
    }
    return 8;
}

int32_t cosc_encode_float32
(
    float value,
    void *data,
    int32_t data_size
)
{
   if (data != COSC_NULL)
   {
       if (data_size < 4)
           return COSC_ESIZE;
       COSC_ENCODE_F32BE_(value, data);
   }
   return 4;
}

int32_t cosc_decode_float32
(
    float *value,
    const void *data,
    int32_t data_size
)
{
    if (data_size < 4)
        return COSC_ESIZE;
    if (value != COSC_NULL)
    {
        COSC_DECODE_F32BE_(*value, data);
    }
    return 4;
}

#ifndef COSC_NOFLOAT64

int32_t cosc_encode_float64
(
    double value,
    void *data,
    int32_t data_size
)
{
   if (data != COSC_NULL)
   {
       if (data_size < 8)
           return COSC_ESIZE;
       COSC_ENCODE_F64BE_(value, data);
   }
   return 8;
}

int32_t cosc_decode_float64
(
    double *value,
    const void *data,
    int32_t data_size
)
{
    if (data_size < 8)
        return COSC_ESIZE;
    if (value != COSC_NULL)
    {
        COSC_DECODE_F64BE_(*value, data);
    }
    return 8;
}

#endif /* FIXME: else clause for 32 bit */

int32_t cosc_encode_string
(
    const char *string,
    int32_t string_size,
    int32_t *string_length,
    void *data,
    int32_t data_size
)
{
    return cosc_encode_string_(string, string_size, string_length, data, data_size);
}

int32_t cosc_decode_string
(
    const char **string,
    int32_t *string_length,
    const void *data,
    int32_t data_size
)
{
    return cosc_decode_string_(string, string_length, data, data_size);
}

int32_t cosc_encode_typetag
(
    const char *typetag,
    int32_t typetag_size,
    int32_t *typetag_length,
    void *data,
    int32_t data_size
)
{
    return cosc_encode_typetag_(typetag, typetag_size, typetag_length, data, data_size);
}

int32_t cosc_decode_typetag
(
    const char **typetag,
    int32_t *typetag_length,
    const void *data,
    int32_t data_size
)
{
    return cosc_decode_typetag_(typetag, typetag_length, data, data_size);
}

int32_t cosc_encode_blob
(
    const void *payload,
    int32_t payload_size,
    void *data,
    int32_t data_size
)
{
    return cosc_encode_blob_(payload, payload_size, data, data_size);
}

int32_t cosc_decode_blob
(
    const void **payload,
    int32_t *payload_size,
    const void *data,
    int32_t data_size
)
{
    return cosc_decode_blob_(payload, payload_size, data, data_size);
}

int32_t cosc_encode_midi
(
    const uint8_t value[4],
    void *data,
    int32_t data_size
)
{
    if (data != COSC_NULL)
    {
        if (data_size < 4)
            return COSC_ESIZE;
        if (value != COSC_NULL)
        {
            COSC_MEMCPY(data, value, 3);
            ((uint8_t *)data)[3] = 0;
        }
        else
            COSC_MEMSET(data, 0, 4);
    }
    return 4;
}

int32_t cosc_decode_midi
(
    uint8_t *value,
    const void *data,
    int32_t data_size
)
{
    if (data_size < 4)
        return COSC_ESIZE;
    if (value != COSC_NULL)
        COSC_MEMCPY(value, data, 3);
    return 4;
}


int32_t cosc_encode_midi_args
(
    uint8_t status,
    uint8_t byte1,
    uint8_t byte2,
    void *data,
    int32_t data_size
)
{
    if (data != COSC_NULL)
    {
        if (data_size < 4)
            return COSC_ESIZE;
        ((uint8_t *)data)[0] = status;
        ((uint8_t *)data)[1] = byte1;
        ((uint8_t *)data)[2] = byte2;
        ((uint8_t *)data)[3] = 0;
    }
    return 4;
}

int32_t cosc_decode_midi_args
(
    uint8_t *status,
    uint8_t *byte1,
    uint8_t *byte2,
    const void *data,
    int32_t data_size
)
{
    if (data_size < 4)
        return COSC_ESIZE;
    if (status != COSC_NULL)
        *status = ((const uint8_t *)data)[0];
    if (byte1 != COSC_NULL)
        *byte1 = ((const uint8_t *)data)[1];
    if (byte2 != COSC_NULL)
        *byte2 = ((const uint8_t *)data)[2];
    return 4;
}


int32_t cosc_encode_midi14
(
    uint8_t status,
    uint32_t value,
    void *data,
    int32_t data_size
)
{
    if (data != COSC_NULL)
    {
        if (data_size < 4)
            return COSC_ESIZE;
        ((uint8_t *)data)[0] = status;
        ((uint8_t *)data)[1] = (value & 0x7f);
        ((uint8_t *)data)[2] = (value & 0x3f80) >> 7;
        ((uint8_t *)data)[3] = 0;
    }
    return 4;
}

int32_t cosc_decode_midi14
(
    uint8_t *status,
    uint32_t *value,
    const void *data,
    int32_t data_size
)
{
    if (data_size < 4)
        return COSC_ESIZE;
    if (status != COSC_NULL)
        *status = ((const uint8_t *)data)[0];
    if (value != COSC_NULL)
        *value = (((const uint8_t *)data)[1] & 0x7f) | (((uint32_t)((const uint8_t *)data)[2] & 0x007f) << 7);
    return 4;
}

int32_t cosc_is_bundle
(
    const void *data,
    int32_t data_size
)
{
    return COSC_IS_BUNDLE_(data, data_size);
}

int32_t cosc_encode_bundle_head
(
    uint64_t timetag,
    int32_t bundle_size,
    void *data,
    int32_t data_size
)
{
    if (data != COSC_NULL)
    {
        if (data_size < 20)
            return COSC_ESIZE;
        if (bundle_size > COSC_DATA_MAX)
            return COSC_EOVERFLOW;
        if (bundle_size < 0)
            bundle_size = 0;
        COSC_MEMCPY(data, "#bundle", 8);
        COSC_ENCODE_U64BE_(timetag, (uint8_t *)data + 8);
        COSC_ENCODE_S32BE_(bundle_size, (uint8_t *)data + 16);
    }
    return 20;
}

int32_t cosc_decode_bundle_head
(
    uint64_t *timetag,
    int32_t *bundle_size,
    const void *data,
    int32_t data_size
)
{
    uint64_t tmp_timetag;
    if (data_size < 20)
        return COSC_ESIZE;
    if (!COSC_IS_BUNDLE_(data, data_size))
        return COSC_EADDRESS;
    COSC_DECODE_U64BE_(tmp_timetag, (const uint8_t *)data + 8);
    if (bundle_size != COSC_NULL)
    {
        uint32_t psize;
        COSC_DECODE_U32BE_(psize, (const uint8_t *)data + 16);
        if (psize > COSC_DATA_MAX)
            return COSC_EOVERFLOW;
        *bundle_size = psize;
    }
    if (timetag != COSC_NULL)
        *timetag = tmp_timetag;
    return 20;
}

int32_t cosc_encode_values
(
    const char *typetag,
    int32_t typetag_size,
    const union cosc_value *values,
    int32_t *num_values,
    void *data,
    int32_t data_size
)
{
    int32_t ret, i;
    int32_t offset = 0, value_count = 0;

    if (typetag_size > 0 && typetag[0] == ',')
    {
        typetag_size--;
        typetag++;
    }

    if (data != COSC_NULL)
    {
        for (i = 0; i < typetag_size && typetag[i] != 0; i++)
        {
            switch (typetag[i])
            {
            case 'i':
                if (data_size < 4) return COSC_ESIZE;
                COSC_ENCODE_S32BE_(values[i].i, data);
                ret = 4;
                break;
            case 'f':
                if (data_size < 4) return COSC_ESIZE;
                COSC_ENCODE_F32BE_(values[i].f, data);
                ret = 4;
                break;
            case 's':
            case 'S':
                ret = cosc_encode_string_(values[i].s.ptr, values[i].s.length, 0, data, data_size);
                break;
            case 'b':
                ret = cosc_encode_blob_(values[i].b.ptr, values[i].b.length, data, data_size);
                break;
            case 'h':
                if (data_size < 8) return COSC_ESIZE;
                COSC_ENCODE_S64BE_(values[i].h, data);
                ret = 8;
                break;
            case 't':
                if (data_size < 8) return COSC_ESIZE;
                COSC_ENCODE_S64BE_(values[i].t, data);
                ret = 8;
                break;
            case 'd':
                if (data_size < 8) return COSC_ESIZE;
                COSC_ENCODE_F64BE_(values[i].d, data);
                ret = 8;
                break;
            case 'c':
                if (data_size < 4) return COSC_ESIZE;
                ((uint8_t *)data)[0] = values[i].c;
                ((uint8_t *)data)[1] = 0;
                ((uint8_t *)data)[2] = 0;
                ((uint8_t *)data)[3] = 0;
                ret = 4;
                break;
            case 'r':
                if (data_size < 4) return COSC_ESIZE;
                COSC_ENCODE_U32BE_(values[i].r, data);
                ret = 4;
                break;
            case 'm':
                if (data_size < 4) return COSC_ESIZE;
                COSC_MEMCPY(data, values[i].m, 3);
                ret = 4;
                break;
            case 'T':
            case 'F':
            case 'N':
            case 'I':
                ret = 0;
                break;
            default:
                return COSC_ETYPETAG;
            }
            if (ret < 0)
                return ret;
            value_count++;
            if (ret == 0)
                continue;
            if (offset > COSC_DATA_MAX - ret) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
            offset += ret;
            data = (uint8_t *)data + ret;
            data_size -= ret;
        }
    }
    else
    {
        for (i = 0; i < typetag_size && typetag[i] != 0; i++)
        {
            switch (typetag[i])
            {
            case 'i':
            case 'f':
            case 'c':
            case 'r':
            case 'm':
                ret = 4;
                break;
            case 's':
            case 'S':
                ret = cosc_encode_string_(values[i].s.ptr, values[i].s.length, 0, 0, 0);
                break;
            case 'b':
                ret = cosc_encode_blob_(values[i].b.ptr, values[i].b.length, 0, 0);
                break;
            case 'h':
            case 't':
            case 'd':
                ret = 8;
                break;
            case 'T':
            case 'F':
            case 'N':
            case 'I':
                ret = 0;
                break;
            default:
                return COSC_ETYPETAG;
            }
            if (offset > COSC_DATA_MAX - ret) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
            offset += ret;
            value_count++;
        }
    }
    if (num_values != COSC_NULL)
        *num_values = value_count;
    return offset;
}

int32_t cosc_decode_values
(
    const char *typetag,
    int32_t typetag_size,
    union cosc_value *values,
    int32_t max_values,
    int32_t *num_values,
    const void *data,
    int32_t data_size
)
{
    int32_t ret, i;
    int32_t offset = 0, value_count = 0;

    if (typetag_size > 0 && typetag[0] == ',')
    {
        typetag_size--;
        typetag++;
    }

    if (values != COSC_NULL)
    {
        for (i = 0; i < typetag_size && typetag[i] != 0; i++)
        {
            switch (typetag[i])
            {
            case 'i':
                if (data_size < 4) return COSC_ESIZE;
                if (i < max_values)
                    COSC_DECODE_S32BE_(values[i].i, data);
                ret = 4;
                break;
            case 'f':
                if (data_size < 4) return COSC_ESIZE;
                if (i < max_values)
                    COSC_DECODE_F32BE_(values[i].f, data);
                ret = 4;
                break;
            case 's':
            case 'S':
                if (i < max_values)
                    ret = cosc_decode_string_(&values[i].s.ptr, &values[i].s.length, data, data_size);
                else
                    ret = cosc_decode_string_(COSC_NULL, COSC_NULL, data, data_size);
                break;
            case 'b':
                if (i < max_values)
                    ret = cosc_decode_blob_(&values[i].b.ptr, &values[i].b.length, data, data_size);
                else
                    ret = cosc_decode_blob_(COSC_NULL, COSC_NULL, data, data_size);
                break;
            case 'h':
                if (data_size < 8) return COSC_ESIZE;
                if (i < max_values)
                    COSC_DECODE_S64BE_(values[i].h, data);
                ret = 8;
                break;
            case 't':
                if (data_size < 8) return COSC_ESIZE;
                if (i < max_values)
                    COSC_DECODE_U64BE_(values[i].t, data);
                ret = 8;
                break;
            case 'd':
                if (data_size < 8) return COSC_ESIZE;
                if (i < max_values)
                    COSC_DECODE_F64BE_(values[i].f, data);
                ret = 8;
                break;
            case 'c':
                if (data_size < 4) return COSC_ESIZE;
                if (i < max_values)
                    values[i].c = *(const uint8_t *)data;
                ret = 4;
                break;
            case 'r':
                if (data_size < 4) return COSC_ESIZE;
                if (i < max_values)
                    COSC_DECODE_U32BE_(values[i].r, data);
                ret = 4;
                break;
            case 'm':
                if (data_size < 4) return COSC_ESIZE;
                if (i < max_values)
                {
                    COSC_MEMCPY(values[i].m, data, 3);
                    values[i].m[3] = 0;
                }
                ret = 4;
                break;
            case 'T':
            case 'F':
                if (i < max_values)
                    values[i].TF = typetag[i] == 'T';
                ret = 0;
                break;
            case 'N':
            case 'I':
                ret = 0;
                break;
            default:
                return COSC_ETYPETAG;
            }
            if (ret < 0)
                return ret;
            value_count++;
            if (ret == 0)
                continue;
            if (offset > COSC_DATA_MAX - ret) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
            offset += ret;
            data_size -= ret;
            data = (const uint8_t *)data + ret;
        }
    }
    else
    {
        for (i = 0; i < typetag_size && typetag[i] != 0; i++)
        {
            switch (typetag[i])
            {
            case 'i':
            case 'f':
            case 'c':
            case 'r':
            case 'm':
                ret = 4;
                break;
            case 's':
            case 'S':
                ret = cosc_decode_string_(COSC_NULL, COSC_NULL, data, data_size);
                break;
            case 'b':
                ret = cosc_decode_blob_(COSC_NULL, COSC_NULL, data, data_size);
                break;
            case 'h':
            case 't':
            case 'd':
                ret = 8;
                break;
            case 'T':
            case 'F':
            case 'N':
            case 'I':
                ret = 0;
                break;
            default:
                return COSC_ETYPETAG;
            }
            if (ret < 0)
                return ret;
            if (ret > data_size)
                return COSC_ESIZE;
            value_count++;
            if (ret == 0)
                continue;
            if (offset > COSC_DATA_MAX - ret) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
            offset += ret;
            data_size -= ret;
            data = (const uint8_t *)data + ret;
        }
    }

    if (num_values != COSC_NULL)
        *num_values = value_count;
    return offset;
}


int32_t cosc_encode_message_head
(
    const char *address,
    int32_t address_n,
    const char *typetag,
    int32_t typetag_n,
    void *data,
    int32_t data_size
)
{
    int32_t offset = 0, ret;
    if (address_n >= 8 && COSC_STRNCMP(address, "#bundle", 8) == 0)
        return COSC_EADDRESS;
    if (data == COSC_NULL)
    {
        ret = cosc_encode_string_(address, address_n, 0, 0, 0);
        if (ret < 0) return ret;  // GCOVR_EXCL_LINE only overflow
        offset += ret;
        ret = cosc_encode_typetag_(typetag, typetag_n, 0, 0, 0);
        if (ret < 0) return ret;  // GCOVR_EXCL_LINE only overflow
        if (offset > COSC_DATA_MAX - ret) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
        offset += ret;
        return offset;
    }
    ret = cosc_encode_string_(address, address_n, 0, data, data_size);
    if (ret < 0)
        return ret;
    offset += ret;
    data_size -= ret;
    ret = cosc_encode_typetag_(typetag, typetag_n, 0, (uint8_t *)data + ret, data_size);
    if (ret < 0)
        return ret;
    if (offset > COSC_DATA_MAX - ret) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
    offset += ret;
    return offset;
}

int32_t cosc_decode_message_head
(
    const char **address,
    int32_t *address_length,
    const char **typetag,
    int32_t *typetag_length,
    const void *data,
    int32_t data_size
)
{
    int32_t offset = 0, ret;
    if (COSC_IS_BUNDLE_(data, data_size))
        return COSC_EADDRESS;
    ret = cosc_decode_string_(address, address_length, data, data_size);
    if (ret < 0)
        return ret;
    offset += ret;
    data_size -= ret;
    ret = cosc_decode_typetag_(typetag, typetag_length, (const uint8_t *)data + offset, data_size);
    if (ret < 0)
        return ret;
    if (offset > COSC_DATA_MAX - ret) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
    offset += ret;
    return offset;
}

int32_t cosc_encode_message
(
    const char *address,
    int32_t address_n,
    const char *typetag,
    int32_t typetag_n,
    const union cosc_value *values,
    int32_t *num_values,
    void *data,
    int32_t data_size
)
{
    int32_t offset = 0, ret;
    if (data == COSC_NULL)
    {
        ret = cosc_encode_message_head(address, address_n, typetag, typetag_n, COSC_NULL, 0);
        if (ret < 0)
            return ret;
        offset += ret;
        ret = cosc_encode_values(typetag, typetag_n, values, num_values, COSC_NULL, 0);
        if (ret < 0)
            return ret;
        if (offset > COSC_DATA_MAX - ret) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
        offset += ret;
        return offset;
    }
    ret = cosc_encode_message_head(address, address_n, typetag, typetag_n, data, data_size);
    if (ret < 0)
        return ret;
    offset += ret;
    data_size -= ret;
    ret = cosc_encode_values(typetag, typetag_n, values, num_values, (uint8_t *)data + offset, data_size);
    if (ret < 0)
        return ret;
    if (offset > COSC_DATA_MAX - ret) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
    offset += ret;
    return offset;
}

int32_t cosc_decode_message
(
    const char **address,
    int32_t *address_length,
    const char **typetag,
    int32_t *typetag_length,
    union cosc_value *values,
    int32_t max_values,
    int32_t *num_values,
    const void *data,
    int32_t data_size
)
{
    int32_t offset = 0, ret;
    const char *tmp_typetag;
    int32_t tmp_typetag_length;
    ret = cosc_decode_message_head(address, address_length, &tmp_typetag, &tmp_typetag_length, data, data_size);
    if (ret < 0)
        return ret;
    offset += ret;
    data_size -= ret;
    // FIXME: max values.
    ret = cosc_decode_values(tmp_typetag, tmp_typetag_length, values, max_values, num_values, (const uint8_t *)data + offset, data_size);
    if (ret < 0)
        return ret;
    if (offset > COSC_DATA_MAX - ret) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
    offset += ret;
    if (typetag != COSC_NULL)
        *typetag = tmp_typetag;
    if (typetag_length != COSC_NULL)
        *typetag_length = tmp_typetag_length;
    return offset;
}

int32_t cosc_find_message_offsets
(
    int32_t *address_length,
    int32_t *typetag_length,
    int32_t *typetag_offset,
    int32_t *values_offset,
    int32_t *index_offset,
    int32_t index,
    const void *data,
    int32_t data_size
)
{
    return cosc_find_message_offsets_(address_length, typetag_length, typetag_offset, values_offset, index_offset, index, data, data_size);
}

int32_t cosc_find_message_pointers
(
    const char **address,
    const char **typetag,
    int32_t index,
    int32_t max_values,
    const void *values[],
    int32_t *num_values,
    const void *data,
    int32_t data_size
)
{
    return cosc_find_message_pointers_(address, typetag, index, max_values, values, num_values, data, data_size);
}

int32_t cosc_modify_set_address
(
    const char *address,
    int32_t address_size,
    int32_t *address_length,
    void *data,
    int32_t data_size
)
{
    /* FIXME: this function might overflow int! */

    int32_t typetag_offset;
    if (COSC_IS_BUNDLE_(data, data_size))
        return COSC_EADDRESS;
    int32_t cursize = cosc_find_message_offsets_(COSC_NULL, COSC_NULL, &typetag_offset, COSC_NULL, COSC_NULL, 0, data, data_size);
    if (cursize < 0)
        return cursize;

    int32_t new_length;
    int32_t new_aligned_size = cosc_encode_string_(address, address_size, &new_length, COSC_NULL, 0);
    if (new_aligned_size < 0) return new_aligned_size;  // GCOVR_EXCL_LINE
    if (new_length == 7 && COSC_MEMCMP(address, "#bundle", 7) == 0)
        return COSC_EADDRESS;

    int32_t diff = new_aligned_size - typetag_offset;
    if (data_size < cursize + diff)
        return COSC_ESIZE;
    if (diff != 0)
        COSC_MEMMOVE((uint8_t *)data + diff + typetag_offset, (uint8_t *)data + typetag_offset, cursize - typetag_offset);

    if (new_length > 0)
        COSC_MEMCPY(data, address, new_length);
    COSC_MEMSET((uint8_t *)data + new_length, 0, new_aligned_size - new_length);

    if (address_length != COSC_NULL)
        *address_length = new_length;

    return cursize + diff;
}

int32_t cosc_modify_append_values
(
    const char *typetag,
    int32_t typetag_size,
    const union cosc_value *values,
    int32_t *num_values,
    void *data,
    int32_t data_size
)
{
    /* FIXME: this function might overflow int! */

    if (typetag_size > 0 && typetag[0] == ',')
    {
        typetag_size--;
        typetag++;
    }

    int32_t old_typetag_length;
    int32_t typetag_offset, old_values_offset;
    int32_t cursize = cosc_find_message_offsets_(COSC_NULL, &old_typetag_length, &typetag_offset, &old_values_offset, COSC_NULL, 0, data, data_size);
    if (cursize < 0)
        return cursize;

    int32_t new_typetag_length = 0;
    while (new_typetag_length < typetag_size && typetag[new_typetag_length] != 0)
        new_typetag_length++;
    if (new_typetag_length >= COSC_DATA_MAX || new_typetag_length >= COSC_DATA_MAX - old_typetag_length) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
    if (new_typetag_length == 0)
        return cursize;

    int32_t combined_typetag_length = new_typetag_length + old_typetag_length;
    int32_t old_typetag_aligned_size = old_values_offset - typetag_offset;
    int32_t new_typetag_aligned_size = COSC_ALIGNED_STRING_SIZE_(combined_typetag_length);
    int32_t typetag_diff = new_typetag_aligned_size - old_typetag_aligned_size;

    int32_t ret_num_values;
    int32_t values_aligned_size = cosc_encode_values(typetag, new_typetag_length, values, &ret_num_values, (uint8_t *)data + cursize + typetag_diff, data_size - typetag_diff);
    if (values_aligned_size < 0)
        return values_aligned_size;

    if (typetag_diff > 0)
        COSC_MEMMOVE((uint8_t *)data + old_values_offset + typetag_diff, (uint8_t *)data + old_values_offset, cursize - old_values_offset);
    COSC_MEMCPY((uint8_t *)data + typetag_offset + old_typetag_length, typetag, new_typetag_length);
    COSC_MEMSET((uint8_t *)data + typetag_offset + combined_typetag_length, 0, new_typetag_aligned_size - combined_typetag_length);

    if (num_values != COSC_NULL)
        *num_values = ret_num_values;

    return cursize + typetag_diff + values_aligned_size;
}

int32_t cosc_modify_insert_values
(
    int32_t index,
    const char *typetag,
    int32_t typetag_size,
    const union cosc_value *values,
    int32_t *num_values,
    void *data,
    int32_t data_size
)
{
    /* FIXME: this function might overflow int! */

    if (typetag_size > 0 && typetag[0] == ',')
    {
        typetag_size--;
        typetag++;
    }

    int32_t old_typetag_length;
    int32_t typetag_offset, old_values_offset, value_index_offset;
    int32_t cursize = cosc_find_message_offsets_(COSC_NULL, &old_typetag_length, &typetag_offset, &old_values_offset, &value_index_offset, index, data, data_size);
    if (cursize < 0)
        return cursize;

    if (((const uint8_t *)data)[typetag_offset] != ',')
        return COSC_ETYPETAG;

    index++;
    if (index >= old_typetag_length || value_index_offset < 0)
        return COSC_ERROR;

    int32_t new_typetag_length = 0;
    while (new_typetag_length < typetag_size && typetag[new_typetag_length] != 0)
        new_typetag_length++;
    if (new_typetag_length >= COSC_DATA_MAX || new_typetag_length >= COSC_DATA_MAX - old_typetag_length) return COSC_EOVERFLOW; // GCOVR_EXCL_LINE
    if (new_typetag_length == 0)
        return cursize;

    int32_t combined_typetag_length = new_typetag_length + old_typetag_length;
    int32_t old_typetag_aligned_size = old_values_offset - typetag_offset;
    int32_t new_typetag_aligned_size = COSC_ALIGNED_STRING_SIZE_(combined_typetag_length);
    int32_t typetag_diff = new_typetag_aligned_size - old_typetag_aligned_size;

    int32_t ret_num_values;
    int32_t values_aligned_size = cosc_encode_values(typetag, new_typetag_length, values, &ret_num_values, COSC_NULL, 0);
    if (values_aligned_size < 0)
        return values_aligned_size;
    int32_t total_added_bytes = typetag_diff + values_aligned_size;
    if (cursize + total_added_bytes > data_size)
        return COSC_ESIZE;

    if (typetag_diff > 0)
        COSC_MEMMOVE((uint8_t *)data + old_values_offset + typetag_diff, (uint8_t *)data + old_values_offset, cursize - old_values_offset);

    if (values_aligned_size > 0)
    {
        COSC_MEMMOVE((uint8_t *)data + value_index_offset + total_added_bytes, (uint8_t *)data + value_index_offset, cursize - value_index_offset);
        values_aligned_size = cosc_encode_values(typetag, new_typetag_length, values, COSC_NULL, (uint8_t *)data + value_index_offset + typetag_diff, data_size - value_index_offset - typetag_diff);
        if (values_aligned_size < 0) return COSC_EBUG; // GCOVR_EXCL_LINE
    }

    COSC_MEMMOVE((uint8_t *)data + typetag_offset + index + new_typetag_length, (uint8_t *)data + typetag_offset + index, old_typetag_length - index);
    COSC_MEMCPY((uint8_t *)data + typetag_offset + index, typetag, new_typetag_length);
    COSC_MEMSET((uint8_t *)data + typetag_offset + combined_typetag_length, 0, new_typetag_aligned_size - combined_typetag_length);

    if (num_values != COSC_NULL)
        *num_values = ret_num_values;

    return cursize + total_added_bytes;
}

int32_t cosc_modify_remove_values
(
    int32_t index,
    int32_t num,
    int32_t *num_values,
    void *data,
    int32_t data_size
)
{
    int32_t old_typetag_length;
    int32_t typetag_offset, old_values_offset, value_index_offset;
    int32_t cursize = cosc_find_message_offsets_(COSC_NULL, &old_typetag_length, &typetag_offset, &old_values_offset, &value_index_offset, index, data, data_size);
    if (cursize < 0)
        return cursize;
    if (num <= 0)
        return cursize;

    const char *typetag = (const char *)data + typetag_offset;

    if (typetag[0] != ',')
        return COSC_ETYPETAG;

    if (value_index_offset < 0)
        return COSC_ERROR;

    index++;
    if (index + num > old_typetag_length)
        num = old_typetag_length - index;

    int32_t remove_values_size = cosc_decode_values(typetag + index, num, COSC_NULL, 0, COSC_NULL, (const uint8_t *)data + value_index_offset, cursize - value_index_offset);
    if (remove_values_size > 0)
        COSC_MEMMOVE((uint8_t *)data + value_index_offset, (uint8_t *)data + value_index_offset + remove_values_size, cursize - value_index_offset - remove_values_size);

    int32_t new_typetag_length = old_typetag_length - num;
    int32_t new_values_offset = typetag_offset + COSC_ALIGNED_STRING_SIZE_(new_typetag_length);
    int32_t typetag_offset_diff = old_values_offset - new_values_offset;

    if (new_values_offset < old_values_offset)
        COSC_MEMMOVE((uint8_t *)data + typetag_offset + index, (uint8_t *)data + typetag_offset + index + num, old_typetag_length - index - num);
    COSC_MEMSET((uint8_t *)data + typetag_offset + new_typetag_length, 0, new_values_offset - typetag_offset - new_typetag_length);
    if (new_values_offset < old_values_offset)
        COSC_MEMMOVE((uint8_t *)data + new_values_offset, (uint8_t *)data + old_values_offset, cursize - old_values_offset - remove_values_size);

    if (num_values != COSC_NULL)
        *num_values = num;

    return cursize - remove_values_size - typetag_offset_diff;
}

int32_t cosc_modify_swap_endian
(
    void *data,
    int32_t data_size
)
{
    uint32_t tmpuint32;
    uint64_t tmpuint64;
    const char *addr, *ttag;
    int32_t addrlen, ttaglen;
    int32_t offset = 0;
    int32_t ret = cosc_decode_string_(&addr, &addrlen, data, data_size);
    if (ret < 0)
        return ret;
    offset += ret;
    ret = cosc_decode_typetag_(&ttag, &ttaglen, (const uint8_t *)data + offset, data_size - offset);
    if (ret < 0)
        return ret;
    offset += ret;

    if (ttag[0] == ',')
    {
        ttag++;
        ttaglen--;
    }

    for (int32_t i = 0; i < ttaglen; i++)
    {
        switch (ttag[i])
        {
        case 'i':
        case 'f':
        case 'r':
            if (data_size - offset >= 4)
            {
                COSC_DECODE_U32BE_(tmpuint32, (uint8_t *)data + offset);
                COSC_MEMCPY((uint8_t *)data + offset, &tmpuint32, 4);
                ret = 4;
            }
            else
                ret = COSC_ESIZE;
            break;
        case 'h':
        case 't':
        case 'd':
            if (data_size - offset >= 8)
            {
                COSC_DECODE_U64BE_(tmpuint64, (uint8_t *)data + offset);
                COSC_MEMCPY((uint8_t *)data + offset, &tmpuint64, 8);
                ret = 8;
            }
            else
                ret = COSC_ESIZE;
            break;
        case 'c':
        case 'm':
            ret = data_size - offset >= 4 ? 4 : COSC_ESIZE;
            break;
        case 's':
        case 'S':
            ret = cosc_decode_string_(COSC_NULL, COSC_NULL, (const uint8_t *)data + offset, data_size - offset);
            break;
        case 'b':
            ret = cosc_decode_blob_(COSC_NULL, COSC_NULL, (const uint8_t *)data + offset, data_size - offset);
            if (ret >= 0)
            {
                COSC_DECODE_U32BE_(tmpuint32, (uint8_t *)data + offset);
                COSC_MEMCPY((uint8_t *)data + offset, &tmpuint32, 4);
            }
            break;
        case 'T':
        case 'F':
        case 'N':
        case 'I':
            ret = 0;
            break;
        default:
            ret = COSC_ETYPETAG;
            break;
        }

        if (ret < 0)
            return ret;
        offset += ret;
    }
    return offset;
}

uint32_t cosc_value_csize
(
    void
)
{
    return sizeof(union cosc_value);
}
