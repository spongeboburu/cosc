/**
 * @example examples.c
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <cosc.h>

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

void dump_value(char type, const union cosc_value *value)
{
    switch (type)
    {
    case 'i':
        printf("    %c: %x\n", type, value->i); break;
    case 'f':
        printf("    %c: %f\n", type, value->f); break;
    case 's':
    case 'S':
        printf("    %c (%d): %s\n", type, value->s.length, value->s.ptr); break;
    case 'b':
        printf("    %c (%d)\n", type, value->b.length); break;
    case 'h':
        printf("    %c: %"PRIx64"\n", type, value->h); break;
    case 't':
        printf("    %c: %"PRIx64"\n", type, value->t); break;
    case 'd':
        printf("    %c: %f\n", type, value->d); break;
    case 'c':
        printf("    %c: %c\n", type, value->c); break;
    case 'r':
        printf("    %c: %x\n", type, value->r); break;
    case 'm':
        printf("    %c: %02x, %02x, %02x, %02x\n", type, value->m[0], value->m[1], value->m[2], value->m[3]); break;
    case 'T':
    case 'F':
        printf("    %c: %s\n", type, value->TF ? "true" : "false"); break;
    default:
        printf("    %c\n", type); break;
    }
}

/**
 * @brief Encode and decode a simple message.
 */
void encode_decode_message(void)
{
    uint8_t bytes[128];
    int32_t num_values, ret, address_length, typetag_length, i;
    const char *address, *typetag;
    union cosc_value values[16] = {
        {.i = 0x01020304},
        {.f = 123.456},
        {.s={.ptr="Yay", .length=3}},
        {.b={.ptr="BLOB", .length=4}},
        {.h = 0x0102030405060708},
        {.t = 0x8070605040302010},
        {.d = 123.456},
        {.S={.ptr="SYM", .length=3}},
        {.c = 'A'},
        {.r = 0xffeeddcc},
        {.m={0x81, 0x40, 0x7f, 0x0}},
    };
    printf("%s\n", "encode_decode_message");

    ret = cosc_encode_message("/theaddress", 1024, "ifsbhtdScrmTFNI", 1024, values, &num_values, bytes, sizeof(bytes));
    if (ret < 0)
    {
        printf("Error encoding message: %s\n", cosc_strerr(ret));
        return;
    }
    printf("Encoded message with %d values to %d bytes\n", num_values, ret);

    // for (int j = 0; j < ret; j++)
    //     printf("%d = 0x%02x '%c'\n", j, bytes[j], bytes[j] > 32 && bytes[j] < 127 ? bytes[j] : 32);

    memset(values, 0, sizeof(values));
    ret = cosc_decode_message(&address, &address_length, &typetag, &typetag_length, values, 16, &num_values, bytes, ret);
    if (ret < 0)
    {
        printf("Error decoding message: %s\n", cosc_strerr(ret));
        return;
    }
    printf("Decoded message with %d values from %d bytes\n", num_values, ret);

    if (typetag[0] != ',')
    {
        printf("Invalid typetag for message\n");
        return;
    }
    typetag++;
    
    printf("Address (%d): %s\n", address_length, address);
    printf("Typetag (%d): %s\n", typetag_length, typetag);
    printf("Values:\n");

    for (i = 0; i < num_values; i++)
    {
        dump_value(typetag[i], values + i);
    }
    printf("\n");
}

/**
 * @brief Encode and decode a bundle.
 */
void encode_decode_bundle(void)
{
    uint8_t bytes[1024];
    int32_t ret, address_length, typetag_length, offset, payload_size;
    const char *address, *typetag;
    union cosc_value value;
    uint64_t timetag;

    printf("%s\n", "encode_decode_bundle");
    offset = 20; // The bundle head takes up exactly 20 bytes.

    value.i = 0x01020304;
    ret = cosc_encode_message("/hey", 1024, ",i", 1024, &value, NULL, bytes + offset, sizeof(bytes) - offset);
    offset += ret; // Could error check.

    value.f = 123.456;
    ret = cosc_encode_message("/way", 1024, ",f", 1024, &value, NULL, bytes + offset, sizeof(bytes) - offset);
    offset += ret; // Could error check.

    ret = cosc_encode_bundle_head(cosc_seconds_to_timetag(654.321), offset - 20, bytes, 20);
    printf("Encoded bundle head to %d bytes, payload %d bytes.\n", ret, offset - 20);

    ret = cosc_decode_bundle_head(&timetag, &payload_size, bytes, sizeof(bytes));
    printf("Decoded bundle head from %d bytes, payload %d bytes.\n", ret, payload_size);
    printf("Timetag: %f\n", cosc_timetag_to_seconds(timetag));
    offset = ret;

    memset(&value, 0, sizeof(value));
    ret = cosc_decode_message(&address, &address_length, &typetag, &typetag_length, &value, 1, NULL, bytes + offset, sizeof(bytes) - offset);
    offset += ret; // Could error check.
    printf("Address (%d): %s\n", address_length, address);
    printf("Typetag (%d): %s\n", typetag_length, typetag);
    printf("Values:\n");
    dump_value(typetag[1], &value);

    memset(&value, 0, sizeof(value));
    ret = cosc_decode_message(&address, &address_length, &typetag, &typetag_length, &value, 1, NULL, bytes + offset, sizeof(bytes) - offset);
    offset += ret; // Could error check.
    printf("Address (%d): %s\n", address_length, address);
    printf("Typetag (%d): %s\n", typetag_length, typetag);
    printf("Values:\n");
    dump_value(typetag[1], &value);
    
    printf("\n");
}

int main(int argc, char *argv[])
{
    encode_decode_message();
    encode_decode_bundle();
    return 0;
}
