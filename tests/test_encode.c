/**
 * Copyright 2021 Peter Gebauer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>
#include <string.h>

#include "cosc.h"

static int setup(void **state)
{
    return 0;
}

static int teardown(void **state)
{
    return 0;
}

static void test_encode_uint8(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0xff;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 4;
    assert_int_equal(data_size, cosc_encode_uint8(0xff, NULL, 0));
    assert_int_equal(data_size, cosc_encode_uint8(0xff, NULL, data_size - 1));
    assert_int_equal(data_size, cosc_encode_uint8(0xff, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    memset(tmps, 0, sizeof(tmps));
    assert_int_equal(data_size, cosc_encode_uint8(0xff, tmps, sizeof(tmps)));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_uint8(0xff, tmps, data_size - 1));
}

static void test_encode_int32(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x01;
    osc_data[1] = 0x02;
    osc_data[2] = 0x03;
    osc_data[3] = 0x04;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 4;
    assert_int_equal(data_size, cosc_encode_int32(0x01020304, NULL, 0));
    assert_int_equal(data_size, cosc_encode_int32(0x01020304, NULL, data_size - 1));
    assert_int_equal(data_size, cosc_encode_int32(0x01020304, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    memset(tmps, 0, sizeof(tmps));
    assert_int_equal(data_size, cosc_encode_int32(0x01020304, tmps, sizeof(tmps)));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_int32(0x01020304, tmps, data_size - 1));
}

static void test_encode_uint32(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0xff;
    osc_data[1] = 0x02;
    osc_data[2] = 0x03;
    osc_data[3] = 0x04;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 4;
    assert_int_equal(data_size, cosc_encode_uint32(0xff020304, NULL, 0));
    assert_int_equal(data_size, cosc_encode_uint32(0xff020304, NULL, data_size - 1));
    assert_int_equal(data_size, cosc_encode_uint32(0xff020304, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    memset(tmps, 0, sizeof(tmps));
    assert_int_equal(data_size, cosc_encode_uint32(0xff020304, tmps, sizeof(tmps)));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_uint32(0xff020304, tmps, data_size - 1));
}

#ifndef COSC_NOINT64

static void test_encode_int64(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x01;
    osc_data[1] = 0x02;
    osc_data[2] = 0x03;
    osc_data[3] = 0x04;
    osc_data[4] = 0x05;
    osc_data[5] = 0x06;
    osc_data[6] = 0x07;
    osc_data[7] = 0x08;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 8;
    assert_int_equal(data_size, cosc_encode_int64(0x0102030405060708, NULL, 0));
    assert_int_equal(data_size, cosc_encode_int64(0x0102030405060708, NULL, data_size - 1));
    assert_int_equal(data_size, cosc_encode_int64(0x0102030405060708, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    memset(tmps, 0, sizeof(tmps));
    assert_int_equal(data_size, cosc_encode_int64(0x0102030405060708, tmps, sizeof(tmps)));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_int64(0x0102030405060708, tmps, data_size - 1));
}

static void test_encode_uint64(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0xff;
    osc_data[1] = 0x02;
    osc_data[2] = 0x03;
    osc_data[3] = 0x04;
    osc_data[4] = 0x05;
    osc_data[5] = 0x06;
    osc_data[6] = 0x07;
    osc_data[7] = 0x08;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 8;
    assert_int_equal(data_size, cosc_encode_uint64(0xff02030405060708, NULL, 0));
    assert_int_equal(data_size, cosc_encode_uint64(0xff02030405060708, NULL, data_size - 1));
    assert_int_equal(data_size, cosc_encode_uint64(0xff02030405060708, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    memset(tmps, 0, sizeof(tmps));
    assert_int_equal(data_size, cosc_encode_uint64(0xff02030405060708, tmps, sizeof(tmps)));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_uint64(0xff02030405060708, tmps, data_size - 1));
}

#endif

static void test_encode_float32(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x42;
    osc_data[1] = 0xf6;
    osc_data[2] = 0xe9;
    osc_data[3] = 0x79;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 4;
    assert_int_equal(data_size, cosc_encode_float32(123.456, NULL, 0));
    assert_int_equal(data_size, cosc_encode_float32(123.456, NULL, data_size - 1));
    assert_int_equal(data_size, cosc_encode_float32(123.456, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    memset(tmps, 0, sizeof(tmps));
    assert_int_equal(data_size, cosc_encode_float32(123.456, tmps, sizeof(tmps)));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_float32(123.456, tmps, data_size - 1));
}

#ifndef COSC_NOFLOAT64

static void test_encode_float64(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x40;
    osc_data[1] = 0x5e;
    osc_data[2] = 0xdd;
    osc_data[3] = 0x2f;
    osc_data[4] = 0x1a;
    osc_data[5] = 0x9f;
    osc_data[6] = 0xbe;
    osc_data[7] = 0x77;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 8;
    assert_int_equal(data_size, cosc_encode_float64(123.456, NULL, 0));
    assert_int_equal(data_size, cosc_encode_float64(123.456, NULL, data_size - 1));
    assert_int_equal(data_size, cosc_encode_float64(123.456, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    memset(tmps, 0, sizeof(tmps));
    assert_int_equal(data_size, cosc_encode_float64(123.456, tmps, sizeof(tmps)));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_float64(123.456, tmps, data_size - 1));
}

#endif

static void test_encode_string(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 'h';
    osc_data[1] = 'e';
    osc_data[2] = 'l';
    osc_data[3] = 'l';
    osc_data[4] = 'o';
    osc_data[5] = 0x00;
    osc_data[6] = 0x00;
    osc_data[7] = 0x00;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 8;
    int32_t stringlen = 0;
    assert_int_equal(data_size, cosc_encode_string("hello", 1024, &stringlen, NULL, 0));
    assert_int_equal(5, stringlen); stringlen = 0;
    assert_int_equal(data_size, cosc_encode_string("hello", 5, &stringlen, NULL, 0));
    assert_int_equal(5, stringlen); stringlen = 0;
    assert_int_equal(data_size, cosc_encode_string("hello", 1024, &stringlen, NULL, data_size - 1));
    assert_int_equal(5, stringlen); stringlen = 0;
    assert_int_equal(data_size, cosc_encode_string("hello", 5, &stringlen, tmps, data_size));
    assert_int_equal(5, stringlen); stringlen = 0;
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    memset(tmps, 0, sizeof(tmps));
    assert_int_equal(data_size, cosc_encode_string("hello", 5, &stringlen, tmps, sizeof(tmps)));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_string("hello", 5, &stringlen, tmps, data_size - 1));

    memset(tmps, 0, sizeof(tmps));
    assert_int_equal(data_size, cosc_encode_string("hello", 1024, NULL, NULL, 0));
    assert_int_equal(data_size, cosc_encode_string("hello", 5, NULL, NULL, 0));
    assert_int_equal(data_size, cosc_encode_string("hello", 1024, NULL, NULL, data_size - 1));
    assert_int_equal(data_size, cosc_encode_string("hello", 5, NULL, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    memset(tmps, 0, sizeof(tmps));
    assert_int_equal(data_size, cosc_encode_string("hello", 5, NULL, tmps, sizeof(tmps)));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_string("hello", 5, NULL, tmps, data_size - 1));

    memset(tmps, 0, sizeof(tmps));
    osc_data[4] = 0;
    assert_int_equal(data_size, cosc_encode_string("hello", 4, &stringlen, NULL, 0));
    assert_int_equal(4, stringlen); stringlen = 0;
    assert_int_equal(data_size, cosc_encode_string("hello", 4, &stringlen, tmps, data_size));
    assert_int_equal(4, stringlen); stringlen = 0;
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    memset(tmps, 0, sizeof(tmps));

    data_size = 4;
    memset(osc_data, 0, sizeof(osc_data));
    osc_data[0] = 'h';
    memset(tmps, 0, sizeof(tmps));
    assert_int_equal(data_size, cosc_encode_string("hello", 1, &stringlen, NULL, 0));
    assert_int_equal(1, stringlen); stringlen = 0;
    assert_int_equal(data_size, cosc_encode_string("hello", 1, &stringlen, tmps, data_size));
    assert_int_equal(1, stringlen); stringlen = 0;
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    memset(tmps, 0, sizeof(tmps));

    assert_int_equal(4, cosc_encode_string("", 1024, NULL, tmps, sizeof(tmps)));
    assert_int_equal(COSC_ESIZE, cosc_encode_string("", 1024, NULL, tmps, 3));
}

static void test_encode_blob(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x00;
    osc_data[1] = 0x00;
    osc_data[2] = 0x00;
    osc_data[3] = 0x04;
    osc_data[4] = 0x01;
    osc_data[5] = 0x02;
    osc_data[6] = 0x03;
    osc_data[7] = 0x04;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 8;

    assert_int_equal(data_size, cosc_encode_blob(osc_data + 4, 4, NULL, 0));
    assert_int_equal(data_size, cosc_encode_blob(osc_data + 4, 4, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_blob(osc_data + 4, 4, tmps, data_size -1));

    memset(osc_data + 4, 0, 4);
    assert_int_equal(data_size, cosc_encode_blob(NULL, 4, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));

    assert_int_equal(4, cosc_encode_blob(NULL, -1, tmps, data_size));
    assert_int_equal(COSC_EOVERFLOW, cosc_encode_blob(NULL, 0x7fffffff, tmps, data_size));
    assert_int_equal(COSC_EOVERFLOW, cosc_encode_blob(NULL, 0x7fffffff, NULL, 0));

    assert_int_equal(12, cosc_encode_blob(NULL, 5, tmps, sizeof(tmps)));
}

static void test_encode_midi(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x81;
    osc_data[1] = 0x40;
    osc_data[2] = 0x7f;
    osc_data[3] = 0x00;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 4;
    uint8_t midi_data[] = {0x81, 0x40, 0x7f, 0};

    assert_int_equal(data_size, cosc_encode_midi(midi_data, NULL, 0));
    assert_int_equal(data_size, cosc_encode_midi(midi_data, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_midi(midi_data, tmps, data_size -1));

    assert_int_equal(data_size, cosc_encode_midi(NULL, tmps, data_size));
    for (int i = 0; i < 4; i++)
        assert_int_equal(0, tmps[i]);
}

static void test_encode_midi_args(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x81;
    osc_data[1] = 0x40;
    osc_data[2] = 0x7f;
    osc_data[3] = 0x00;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 4;

    assert_int_equal(data_size, cosc_encode_midi_args(0x81, 0x40, 0x7f, NULL, 0));
    assert_int_equal(data_size, cosc_encode_midi_args(0x81, 0x40, 0x7f, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_midi_args(0x81, 0x40, 0x7f, tmps, data_size -1));
}

static void test_encode_midi14(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x81;
    osc_data[1] = 0x20;
    osc_data[2] = 0x40;
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 4;

    assert_int_equal(data_size, cosc_encode_midi14(0x81, 0x2020, NULL, 0));
    assert_int_equal(data_size, cosc_encode_midi14(0x81, 0x2020, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_midi14(0x81, 0x2020, tmps, data_size -1));
}

#ifndef COSC_NOINT64

static void test_encode_bundle_head(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = '#';
    osc_data[1] = 'b';
    osc_data[2] = 'u';
    osc_data[3] = 'n';
    osc_data[4] = 'd';
    osc_data[5] = 'l';
    osc_data[6] = 'e';
    osc_data[7] = 0;
    osc_data[8] = 0xff;
    osc_data[9] = 0x02;
    osc_data[10] = 0x03;
    osc_data[11] = 0x04;
    osc_data[12] = 0x05;
    osc_data[13] = 0x06;
    osc_data[14] = 0x07;
    osc_data[15] = 0x08;
    osc_data[16] = 0x01;
    osc_data[17] = 0x02;
    osc_data[18] = 0x03;
    osc_data[19] = 0x04;
    
    uint8_t tmps[sizeof(osc_data)] = {0};
    int data_size = 20;

    assert_int_equal(data_size, cosc_encode_bundle_head(0xff02030405060708, 0x01020304, NULL, 0));
    assert_int_equal(data_size, cosc_encode_bundle_head(0xff02030405060708, 0x01020304, tmps, data_size));
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(20, cosc_encode_bundle_head(0xff02030405060708, -1, tmps, data_size));
    assert_int_equal(COSC_ESIZE, cosc_encode_bundle_head(0xff02030405060708, 0x01020304, tmps, data_size -1));
    assert_int_equal(COSC_EOVERFLOW, cosc_encode_bundle_head(0xff02030405060708, 0x7fffffff, tmps, data_size));
}

#endif

static void test_encode_values(void **state)
{
    uint8_t osc_data[128] = {0};
    memcpy(osc_data, (uint8_t[]){0x01, 0x02, 0x03, 0x04}, 4);
    memcpy(osc_data + 4, (uint8_t[]){0x42, 0xf6, 0xe9, 0x79}, 4);
    memcpy(osc_data + 8, "Yay", 3);
    memcpy(osc_data + 12, (uint8_t[]){0x00, 0x00, 0x00, 0x04}, 4);
    memcpy(osc_data + 16, (uint8_t[]){'B', 'L', 'O', 'B'}, 4);
#ifndef COSC_NOINT64
    memcpy(osc_data + 20, (uint8_t[]){0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}, 8);
    memcpy(osc_data + 28, (uint8_t[]){0xff, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}, 8);
#endif
#ifndef COSC_NOFLOAT64
    memcpy(osc_data + 36, (uint8_t[]){0x40, 0x5e, 0xdd, 0x2f, 0x1a, 0x9f, 0xbe, 0x77}, 8);
#endif
    memcpy(osc_data + 44, "SYM", 3);
    memcpy(osc_data + 48, "C", 1);
    memcpy(osc_data + 52, (uint8_t[]){0x0ff, 0x02, 0x03, 0x04}, 4);
    memcpy(osc_data + 56, (uint8_t[]){0x081, 0x40, 0x7f, 0x00}, 4);

    union cosc_value values[16] = {0};
    values[0].i = 0x01020304;
    values[1].f = 123.456;
    values[2].s.ptr = "Yay";
    values[2].s.length = 3;
    values[3].b.ptr = "BLOB";
    values[3].b.length = 4;
    values[4].h = 0x0102030405060708;
    values[5].t = 0xff02030405060708;
    values[6].d = 123.456;
    values[7].S.ptr = "SYM";
    values[7].S.length = 3;
    values[8].c = 'C';
    values[9].r = 0xff020304;
    memcpy(values[10].m, (uint8_t[]){0x81, 0x40, 0x7f}, 3);

    uint8_t tmps[sizeof(osc_data)] = {0};
    int32_t num_values = 0;

    const char *ttag = ",ifsbhtdScrmTFNI";
    
    assert_int_equal(60, cosc_encode_values(ttag, 1024, values, &num_values, NULL, 0));
    assert_int_equal(15, num_values); num_values = 0;
    assert_int_equal(60, cosc_encode_values(ttag, 1024, values, &num_values, tmps, sizeof(tmps)));
    assert_int_equal(15, num_values); num_values = 0;
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_values(ttag, 1024, values, &num_values, tmps, 59));
    values[0].s.ptr = "Hello World!";
    values[0].s.length = 12;
    assert_int_equal(COSC_ESIZE, cosc_encode_values(",s", 1024, values, NULL, tmps, 12));

    memset(values, 0, sizeof(values));
    assert_int_equal(0, cosc_encode_values(",s", 0, values, NULL, tmps, sizeof(tmps)));
    assert_int_equal(COSC_ESIZE, cosc_encode_values("i", 1, values, NULL, tmps, 3));
    assert_int_equal(COSC_ESIZE, cosc_encode_values("f", 1, values, NULL, tmps, 3));
    assert_int_equal(COSC_ESIZE, cosc_encode_values("h", 1, values, NULL, tmps, 7));
    assert_int_equal(COSC_ESIZE, cosc_encode_values("t", 1, values, NULL, tmps, 7));
    assert_int_equal(COSC_ESIZE, cosc_encode_values("d", 1, values, NULL, tmps, 7));
    assert_int_equal(COSC_ESIZE, cosc_encode_values("c", 1, values, NULL, tmps, 3));
    assert_int_equal(COSC_ESIZE, cosc_encode_values("r", 1, values, NULL, tmps, 3));
    assert_int_equal(4, cosc_encode_values("b", 1, values, NULL, NULL, 0));
    values[0].b.length = 0x7fffffff;
    assert_int_equal(COSC_EOVERFLOW, cosc_encode_values("b", 1, values, NULL, NULL, 0));
}

static void test_encode_typetag(void **state)
{
    uint8_t tmps[128] = {0};
    uint8_t osc_data[128] = {',', 'i', 'f', 's', 'b', 0, 0, 0};
    int32_t typetag_length = 0;

    assert_int_equal(4, cosc_encode_typetag("", 1024, NULL, NULL, 0));
    assert_int_equal(8, cosc_encode_typetag(",ifsb", 5, NULL, NULL, 0));
    assert_int_equal(8, cosc_encode_typetag(",ifsb", 1024, NULL, NULL, 0));
    assert_int_equal(8, cosc_encode_typetag(",ifsb", 1024, &typetag_length, tmps, 8));
    assert_int_equal(5, typetag_length);
    assert_memory_equal(osc_data, tmps, 8);
    assert_int_equal(COSC_ESIZE, cosc_encode_typetag(",ifsb", 1024, NULL, tmps, 7));

    assert_int_equal(4, cosc_encode_typetag(",", 1024, NULL, tmps, sizeof(tmps)));
    assert_int_equal(8, cosc_encode_typetag(",ifsb", 5, NULL, tmps, sizeof(tmps)));
    assert_int_equal(4, cosc_encode_typetag("", 1024, &typetag_length, tmps, 8));
    assert_int_equal(1, typetag_length);
    assert_int_equal(4, cosc_encode_typetag("i", 1024, &typetag_length, tmps, 8));
    assert_int_equal(2, typetag_length);
    assert_int_equal(COSC_ESIZE, cosc_encode_typetag(",ifsb", 1024, NULL, tmps, 1));
}

static void test_encode_message_head(void **state)
{
    uint8_t tmps[128] = {0};

    assert_int_equal(16, cosc_encode_message_head("abcd", 1024, ",ifsb", 1024, NULL, 0));
    assert_int_equal(16, cosc_encode_message_head("abcd", 1024, ",ifsb", 1024, tmps, 128));

    const uint8_t test1[] = {'a', 'b', 'c', 'd', 0, 0, 0, 0};
    const uint8_t test2[] = {',', 'i', 'f', 's', 'b', 0, 0, 0};

    assert_memory_equal(test1, tmps, 8);
    assert_memory_equal(test2, tmps + 8, 8);
    
    assert_int_equal(COSC_ESIZE, cosc_encode_message_head("abcd", 1024, ",ifsb", 1024, tmps, 15));
    assert_int_equal(COSC_EADDRESS, cosc_encode_message_head("#bundle", 1024, ",ifsb", 1024, tmps, sizeof(tmps)));
    assert_int_equal(16, cosc_encode_message_head("#bundle", 4, ",ifsb", 1024, tmps, sizeof(tmps)));
}

static void test_encode_message(void **state)
{
    uint8_t osc_data[128] = {0};
    memcpy(osc_data, "abcd", 4);
    memcpy(osc_data + 8, ",ifsbhtdScrmTFNI", 16);
    memcpy(osc_data + 28, (uint8_t[]){0x01, 0x02, 0x03, 0x04}, 4);
    memcpy(osc_data + 28 + 4, (uint8_t[]){0x42, 0xf6, 0xe9, 0x79}, 4);
    memcpy(osc_data + 28 + 8, "Yay", 3);
    memcpy(osc_data + 28 + 12, (uint8_t[]){0x00, 0x00, 0x00, 0x04}, 4);
    memcpy(osc_data + 28 + 16, (uint8_t[]){'B', 'L', 'O', 'B'}, 4);
    memcpy(osc_data + 28 + 20, (uint8_t[]){0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}, 8);
    memcpy(osc_data + 28 + 28, (uint8_t[]){0xff, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}, 8);
    memcpy(osc_data + 28 + 36, (uint8_t[]){0x40, 0x5e, 0xdd, 0x2f, 0x1a, 0x9f, 0xbe, 0x77}, 8);
    memcpy(osc_data + 28 + 44, "SYM", 3);
    memcpy(osc_data + 28 + 48, "C", 1);
    memcpy(osc_data + 28 + 52, (uint8_t[]){0x0ff, 0x02, 0x03, 0x04}, 4);
    memcpy(osc_data + 28 + 56, (uint8_t[]){0x081, 0x40, 0x7f, 0x00}, 4);

    union cosc_value values[16];
    values[0].i = 0x01020304;
    values[1].f = 123.456;
    values[2].s.ptr = "Yay";
    values[2].s.length = 3;
    values[3].b.ptr = "BLOB";
    values[3].b.length = 4;
    values[4].h = 0x0102030405060708;
    values[5].t = 0xff02030405060708;
    values[6].d = 123.456;
    values[7].S.ptr = "SYM";
    values[7].S.length = 3;
    values[8].c = 'C';
    values[9].r = 0xff020304;
    memcpy(values[10].m, (uint8_t[]){0x81, 0x40, 0x7f}, 3);

    uint8_t tmps[sizeof(osc_data)] = {0};
    int32_t num_values = 0;
    int data_size = 88;

    assert_int_equal(data_size, cosc_encode_message("abcd", 1024, ",ifsbhtdScrmTFNI", 1024, values, &num_values, NULL, 0));
    assert_int_equal(15, num_values); num_values = 0;
    assert_int_equal(data_size, cosc_encode_message("abcd", 1024, ",ifsbhtdScrmTFNI", 1024, values, &num_values, tmps, sizeof(tmps)));
    assert_int_equal(15, num_values); num_values = 0;
    assert_memory_equal(osc_data, tmps, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_message("abcd", 1024, ",ifsbhtdScrmTFNI", 1024, values, &num_values, tmps, data_size - 1));
    assert_int_equal(COSC_ESIZE, cosc_encode_message("abcd", 1024, ",x", 1024, values, &num_values, tmps, 1));
    assert_int_equal(COSC_EADDRESS, cosc_encode_message("#bundle", 1024, ",ifsb", 1024, values, &num_values, NULL, 0));
    assert_int_equal(COSC_ETYPETAG, cosc_encode_message("abcd", 1024, ",x", 1024, values, &num_values, NULL, 0));
}

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_encode_uint8),
        cmocka_unit_test(test_encode_int32),
        cmocka_unit_test(test_encode_uint32),
        cmocka_unit_test(test_encode_int64),
        cmocka_unit_test(test_encode_uint64),
        cmocka_unit_test(test_encode_float32),
        cmocka_unit_test(test_encode_float64),
        cmocka_unit_test(test_encode_string),
        cmocka_unit_test(test_encode_blob),
        cmocka_unit_test(test_encode_midi),
        cmocka_unit_test(test_encode_midi_args),
        cmocka_unit_test(test_encode_midi14),
        cmocka_unit_test(test_encode_bundle_head),
        cmocka_unit_test(test_encode_values),
        cmocka_unit_test(test_encode_typetag),
        cmocka_unit_test(test_encode_message_head),
        cmocka_unit_test(test_encode_message),
    };

    return cmocka_run_group_tests(tests, setup, teardown);
}
