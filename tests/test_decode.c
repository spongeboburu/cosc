/**
 * Copyright 2021 Peter Gebauer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>
#include <string.h>

#include "cosc.h"

static int setup(void **state)
{
    return 0;
}

static int teardown(void **state)
{
    return 0;
}

static void test_decode_uint8(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0xff;
    uint8_t value = 0;
    int data_size = 4;
    assert_int_equal(data_size, cosc_decode_uint8(NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_uint8(&value, osc_data, data_size));
    assert_int_equal(0xff, value);
    value = 0;
    assert_int_equal(data_size, cosc_decode_uint8(&value, osc_data, sizeof(osc_data)));
    assert_int_equal(0xff, value);
    assert_int_equal(COSC_ESIZE, cosc_decode_uint8(NULL, osc_data, data_size - 1));
    value = 0;
    assert_int_equal(COSC_ESIZE, cosc_decode_uint8(&value, osc_data, data_size - 1));
    assert_int_equal(0, value);
}

static void test_decode_int32(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x01;
    osc_data[1] = 0x02;
    osc_data[2] = 0x03;
    osc_data[3] = 0x04;
    int32_t value = 0;
    int data_size = 4;
    assert_int_equal(data_size, cosc_decode_int32(NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_int32(&value, osc_data, data_size));
    assert_int_equal(0x01020304, value);
    value = 0;
    assert_int_equal(data_size, cosc_decode_int32(&value, osc_data, sizeof(osc_data)));
    assert_int_equal(0x01020304, value);
    assert_int_equal(COSC_ESIZE, cosc_decode_int32(NULL, osc_data, data_size - 1));
    value = 0;
    assert_int_equal(COSC_ESIZE, cosc_decode_int32(&value, osc_data, data_size - 1));
    assert_int_equal(0, value);
}

static void test_decode_uint32(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0xff;
    osc_data[1] = 0x02;
    osc_data[2] = 0x03;
    osc_data[3] = 0x04;
    uint32_t value = 0;
    int data_size = 4;
    assert_int_equal(data_size, cosc_decode_uint32(NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_uint32(&value, osc_data, data_size));
    assert_int_equal(0xff020304, value);
    value = 0;
    assert_int_equal(data_size, cosc_decode_uint32(&value, osc_data, sizeof(osc_data)));
    assert_int_equal(0xff020304, value);
    assert_int_equal(COSC_ESIZE, cosc_decode_uint32(NULL, osc_data, data_size - 1));
    value = 0;
    assert_int_equal(COSC_ESIZE, cosc_decode_uint32(&value, osc_data, data_size - 1));
    assert_int_equal(0, value);
}

static void test_decode_int64(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x01;
    osc_data[1] = 0x02;
    osc_data[2] = 0x03;
    osc_data[3] = 0x04;
    osc_data[4] = 0x05;
    osc_data[5] = 0x06;
    osc_data[6] = 0x07;
    osc_data[7] = 0x08;
    int64_t value = 0;
    int data_size = 8;
    assert_int_equal(data_size, cosc_decode_int64(NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_int64(&value, osc_data, data_size));
    assert_int_equal(0x0102030405060708, value);
    value = 0;
    assert_int_equal(data_size, cosc_decode_int64(&value, osc_data, sizeof(osc_data)));
    assert_int_equal(0x0102030405060708, value);
    assert_int_equal(COSC_ESIZE, cosc_decode_int64(NULL, osc_data, data_size - 1));
    value = 0;
    assert_int_equal(COSC_ESIZE, cosc_decode_int64(&value, osc_data, data_size - 1));
    assert_int_equal(0, value);
}

static void test_decode_uint64(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0xff;
    osc_data[1] = 0x02;
    osc_data[2] = 0x03;
    osc_data[3] = 0x04;
    osc_data[4] = 0x05;
    osc_data[5] = 0x06;
    osc_data[6] = 0x07;
    osc_data[7] = 0x08;
    uint64_t value = 0;
    int data_size = 8;
    assert_int_equal(data_size, cosc_decode_uint64(NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_uint64(&value, osc_data, data_size));
    assert_int_equal(0xff02030405060708, value);
    value = 0;
    assert_int_equal(data_size, cosc_decode_uint64(&value, osc_data, sizeof(osc_data)));
    assert_int_equal(0xff02030405060708, value);
    assert_int_equal(COSC_ESIZE, cosc_decode_uint64(NULL, osc_data, data_size - 1));
    value = 0;
    assert_int_equal(COSC_ESIZE, cosc_decode_uint64(&value, osc_data, data_size - 1));
    assert_int_equal(0, value);
}

static void test_decode_float32(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x42;
    osc_data[1] = 0xf6;
    osc_data[2] = 0xe9;
    osc_data[3] = 0x79;
    float value = 0;
    int data_size = 4;
    assert_int_equal(data_size, cosc_decode_float32(NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_float32(&value, osc_data, data_size));
    assert_float_equal(123.456, value, 0.0001);
    value = 0;
    assert_int_equal(data_size, cosc_decode_float32(&value, osc_data, sizeof(osc_data)));
    assert_float_equal(123.456, value, 0.0001);
    assert_int_equal(COSC_ESIZE, cosc_decode_float32(NULL, osc_data, data_size - 1));
    value = 0;
    assert_int_equal(COSC_ESIZE, cosc_decode_float32(&value, osc_data, data_size - 1));
    assert_int_equal(0, value);
}

static void test_decode_float64(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x40;
    osc_data[1] = 0x5e;
    osc_data[2] = 0xdd;
    osc_data[3] = 0x2f;
    osc_data[4] = 0x1a;
    osc_data[5] = 0x9f;
    osc_data[6] = 0xbe;
    osc_data[7] = 0x77;
    double value = 0;
    int data_size = 8;
    assert_int_equal(data_size, cosc_decode_float64(NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_float64(&value, osc_data, data_size));
    assert_float_equal(123.456, value, 0.0001);
    value = 0;
    assert_int_equal(data_size, cosc_decode_float64(&value, osc_data, sizeof(osc_data)));
    assert_float_equal(123.456, value, 0.0001);
    assert_int_equal(COSC_ESIZE, cosc_decode_float64(NULL, osc_data, data_size - 1));
    value = 0;
    assert_int_equal(COSC_ESIZE, cosc_decode_float64(&value, osc_data, data_size - 1));
    assert_int_equal(0, value);
}

static void test_decode_string(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 'h';
    osc_data[1] = 'e';
    osc_data[2] = 'l';
    osc_data[3] = 'l';
    osc_data[4] = 'o';
    osc_data[5] = 0x00;
    osc_data[6] = 0x00;
    osc_data[7] = 0x00;
    const char *value = NULL;
    int data_size = 8;
    int32_t stringlen = 0;

    assert_int_equal(data_size, cosc_decode_string(NULL, NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_string(&value, &stringlen, osc_data, data_size));
    assert_int_equal(5, stringlen); stringlen = 0;
    assert_non_null(value);
    assert_memory_equal(osc_data, value, data_size); value = NULL;
    assert_int_equal(COSC_ESIZE, cosc_decode_string(NULL, NULL, osc_data, data_size -1));
    assert_int_equal(COSC_ESIZE, cosc_decode_string(&value, &stringlen, osc_data, data_size - 1));

    assert_int_equal(COSC_ESIZE, cosc_decode_string(&value, &stringlen, osc_data, 3));
}

static void test_decode_blob(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x00;
    osc_data[1] = 0x00;
    osc_data[2] = 0x00;
    osc_data[3] = 0x04;
    osc_data[4] = 0x01;
    osc_data[5] = 0x02;
    osc_data[6] = 0x03;
    osc_data[7] = 0x04;
    const void *value = NULL;
    int data_size = 8;
    int32_t blobsize = 0;

    assert_int_equal(data_size, cosc_decode_blob(NULL, NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_blob(&value, &blobsize, osc_data, data_size));
    assert_int_equal(4, blobsize);
    assert_non_null(value);
    assert_memory_equal(osc_data + 4, value, 4); value = NULL;
    assert_int_equal(COSC_ESIZE, cosc_decode_blob(NULL, NULL, osc_data, data_size -1));
    assert_int_equal(COSC_ESIZE, cosc_decode_blob(&value, &blobsize, osc_data, data_size -1));
    assert_int_equal(COSC_ESIZE, cosc_decode_blob(NULL, &blobsize, osc_data, 1));
    osc_data[0] = 0xff;
    assert_int_equal(COSC_EOVERFLOW, cosc_decode_blob(NULL, &blobsize, osc_data, 8));
}

static void test_decode_midi(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x81;
    osc_data[1] = 0x40;
    osc_data[2] = 0x7f;
    osc_data[3] = 0x00;
    int data_size = 4;
    uint8_t midi_data[128] = {0};

    assert_int_equal(data_size, cosc_decode_midi(NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_midi(midi_data, osc_data, data_size));
    assert_memory_equal(osc_data, midi_data, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, cosc_encode_midi(midi_data, osc_data, data_size -1));
    assert_int_equal(COSC_ESIZE, cosc_decode_midi(midi_data, osc_data, 3));
}

static void test_decode_midi_args(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x81;
    osc_data[1] = 0x40;
    osc_data[2] = 0x7f;
    osc_data[3] = 0x00;
    int data_size = 4;
    uint8_t status = 0, byte1 = 0, byte2 = 0;

    assert_int_equal(data_size, cosc_decode_midi_args(NULL, NULL, NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_midi_args(&status, &byte1, &byte2, osc_data, data_size));
    assert_int_equal(0x81, status);
    assert_int_equal(0x40, byte1);
    assert_int_equal(0x7f, byte2);
    assert_int_equal(COSC_ESIZE, cosc_decode_midi_args(&status, &byte1, &byte2, osc_data, data_size -1));
}

static void test_decode_midi14(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = 0x81;
    osc_data[1] = 0x20;
    osc_data[2] = 0x40;
    int data_size = 4;
    uint8_t status = 0;
    uint32_t value = 0;

    assert_int_equal(data_size, cosc_decode_midi14(NULL, NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_midi14(&status, &value, osc_data, data_size));
    assert_int_equal(0x81, status);
    assert_int_equal(0x2020, value);
    assert_int_equal(COSC_ESIZE, cosc_decode_midi14(&status, &value, osc_data, data_size -1));
}

static void test_decode_bundle_head(void **state)
{
    uint8_t osc_data[128] = {0};
    osc_data[0] = '#';
    osc_data[1] = 'b';
    osc_data[2] = 'u';
    osc_data[3] = 'n';
    osc_data[4] = 'd';
    osc_data[5] = 'l';
    osc_data[6] = 'e';
    osc_data[7] = 0;
    osc_data[8] = 0xff;
    osc_data[9] = 0x02;
    osc_data[10] = 0x03;
    osc_data[11] = 0x04;
    osc_data[12] = 0x05;
    osc_data[13] = 0x06;
    osc_data[14] = 0x07;
    osc_data[15] = 0x08;
    osc_data[16] = 0x01;
    osc_data[17] = 0x02;
    osc_data[18] = 0x03;
    osc_data[19] = 0x04;
    
    int data_size = 20;
    uint64_t timestamp = 0;
    int32_t bundle_size = 0;

    assert_int_equal(data_size, cosc_decode_bundle_head(NULL, NULL, osc_data, data_size));
    assert_int_equal(data_size, cosc_decode_bundle_head(&timestamp, &bundle_size, osc_data, data_size));
    assert_int_equal(0xff02030405060708, timestamp);
    assert_int_equal(0x01020304, bundle_size);
    assert_int_equal(COSC_ESIZE, cosc_decode_bundle_head(&timestamp, &bundle_size, osc_data, data_size -1));

    osc_data[0] = 'x';
    assert_int_equal(COSC_EADDRESS, cosc_decode_bundle_head(&timestamp, &bundle_size, osc_data, data_size));
    osc_data[0] = '#';

    osc_data[16] = 0x7f;
    osc_data[17] = 0xff;
    osc_data[18] = 0xff;
    osc_data[19] = 0xff;
    assert_int_equal(COSC_EOVERFLOW, cosc_decode_bundle_head(&timestamp, &bundle_size, osc_data, data_size));
}

static void test_decode_values(void **state)
{
    uint8_t osc_data[128] = {0};
    memcpy(osc_data, (uint8_t[]){0x01, 0x02, 0x03, 0x04}, 4);
    memcpy(osc_data + 4, (uint8_t[]){0x42, 0xf6, 0xe9, 0x79}, 4);
    memcpy(osc_data + 8, "Yay", 3);
    memcpy(osc_data + 12, (uint8_t[]){0x00, 0x00, 0x00, 0x04}, 4);
    memcpy(osc_data + 16, (uint8_t[]){'B', 'L', 'O', 'B'}, 4);
    memcpy(osc_data + 20, (uint8_t[]){0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}, 8);
    memcpy(osc_data + 28, (uint8_t[]){0xff, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}, 8);
    memcpy(osc_data + 36, (uint8_t[]){0x40, 0x5e, 0xdd, 0x2f, 0x1a, 0x9f, 0xbe, 0x77}, 8);
    memcpy(osc_data + 44, "SYM", 3);
    memcpy(osc_data + 48, "C", 1);
    memcpy(osc_data + 52, (uint8_t[]){0x0ff, 0x02, 0x03, 0x04}, 4);
    memcpy(osc_data + 56, (uint8_t[]){0x081, 0x40, 0x7f, 0x00}, 4);

    union cosc_value values[16] = {0};
    values[0].i = 0x01020304;
    values[1].f = 123.456;
    values[2].s.ptr = "Yay";
    values[2].s.length = 3;
    values[3].b.ptr = "BLOB";
    values[3].b.length = 4;
    values[4].h = 0x0102030405060708;
    values[5].t = 0xff02030405060708;
    values[6].d = 123.456;
    values[7].S.ptr = "SYM";
    values[7].S.length = 3;
    values[8].c = 'C';
    values[9].r = 0xff020304;
    memcpy(values[10].m, (uint8_t[]){0x81, 0x40, 0x7f}, 3);

    union cosc_value tmp_values[16];
    int32_t num_values = 0;

    assert_int_equal(60, cosc_decode_values(",ifsbhtdScrmTFNI", 1024, NULL, 16, NULL, osc_data, 60));
    assert_int_equal(60, cosc_decode_values(",ifsbhtdScrmTFNI", 1024, tmp_values, 16, &num_values, osc_data, 60));
    assert_int_equal(15, num_values); num_values = 0;

    assert_int_equal(values[0].i, tmp_values[0].i);
    assert_float_equal(values[1].f, tmp_values[1].f, 0.0001);
    assert_non_null(tmp_values[2].s.ptr);
    assert_int_equal(values[2].s.length, tmp_values[2].s.length);
    assert_string_equal(values[2].s.ptr, tmp_values[2].s.ptr);
    assert_non_null(tmp_values[3].b.ptr);
    assert_int_equal(values[3].b.length, tmp_values[3].b.length);
    assert_memory_equal(values[3].b.ptr, tmp_values[3].b.ptr, tmp_values[3].b.length);
    assert_int_equal(values[4].h, tmp_values[4].h);
    assert_int_equal(values[5].t, tmp_values[5].t);
    assert_float_equal(values[6].d, tmp_values[6].d, 0.0001);
    assert_non_null(tmp_values[7].s.ptr);
    assert_int_equal(values[7].S.length, tmp_values[7].S.length);
    assert_string_equal(values[7].S.ptr, tmp_values[7].S.ptr);
    assert_int_equal(values[8].c, tmp_values[8].c);
    assert_int_equal(values[9].r, tmp_values[9].r);
    assert_memory_equal(values[10].m, tmp_values[10].m, 3);
    assert_int_equal(60, cosc_decode_values(",ifsbhtdScrmTFNI", 1024, values, 1, &num_values, osc_data, 60));
    assert_int_equal(15, num_values); num_values = 0;
    
    assert_int_equal(COSC_ESIZE, cosc_decode_values(",ifsbhtdScrmTFNI", 1024, values, 16, &num_values, osc_data, 59));
    assert_int_equal(COSC_ESIZE, cosc_decode_values(",s", 1024, values, 1, &num_values, osc_data, 10));
    assert_int_equal(COSC_ESIZE, cosc_decode_values(",s", 1024, NULL, 1, &num_values, osc_data, 10));

    assert_int_equal(0, cosc_decode_values(",s", 0, NULL, 1, &num_values, osc_data, sizeof(osc_data)));

    memset(osc_data, 0, sizeof(osc_data));
    assert_int_equal(4, cosc_decode_values(",s", 2, NULL, 1, &num_values, osc_data, sizeof(osc_data)));
    assert_int_equal(0, cosc_decode_values(",N", 2, NULL, 1, &num_values, osc_data, sizeof(osc_data)));
    assert_int_equal(COSC_ESIZE, cosc_decode_values("i", 1, values, 1, &num_values, osc_data, 3));
    assert_int_equal(4, cosc_decode_values("i", 1, values, 0, &num_values, osc_data, 4));
    assert_int_equal(COSC_ESIZE, cosc_decode_values("f", 1, values, 1, &num_values, osc_data, 3));
    assert_int_equal(4, cosc_decode_values("f", 1, values, 0, &num_values, osc_data, 4));
    assert_int_equal(COSC_ESIZE, cosc_decode_values("h", 1, values, 1, &num_values, osc_data, 7));
    assert_int_equal(8, cosc_decode_values("h", 1, values, 0, &num_values, osc_data, 8));
    assert_int_equal(COSC_ESIZE, cosc_decode_values("t", 1, values, 1, &num_values, osc_data, 7));
    assert_int_equal(8, cosc_decode_values("t", 1, values, 0, &num_values, osc_data, 8));
    assert_int_equal(COSC_ESIZE, cosc_decode_values("d", 1, values, 1, &num_values, osc_data, 7));
    assert_int_equal(8, cosc_decode_values("d", 1, values, 0, &num_values, osc_data, 8));
    assert_int_equal(COSC_ESIZE, cosc_decode_values("c", 1, values, 1, &num_values, osc_data, 3));
    assert_int_equal(4, cosc_decode_values("c", 1, values, 0, &num_values, osc_data, 4));
    assert_int_equal(COSC_ESIZE, cosc_decode_values("r", 1, values, 1, &num_values, osc_data, 3));
    assert_int_equal(4, cosc_decode_values("r", 1, values, 0, &num_values, osc_data, 4));

    assert_int_equal(COSC_ETYPETAG, cosc_decode_values("x", 1, values, 0, &num_values, osc_data, 4));
    assert_int_equal(COSC_ETYPETAG, cosc_decode_values("x", 1, NULL, 0, &num_values, osc_data, 4));

    assert_int_equal(COSC_ESIZE, cosc_decode_values("i", 1, NULL, 0, &num_values, osc_data, 2));
}

static void test_decode_typetag(void **state)
{
    uint8_t osc_data[128] = {',', 'i', 'f', 's', 'b', 0, 0, 0};
    const char *typetag = NULL;
    int32_t typetag_length = 0;

    assert_int_equal(8, cosc_decode_typetag(NULL, NULL, osc_data, 8));
    assert_int_equal(8, cosc_decode_typetag(&typetag, &typetag_length, osc_data, 8));
    assert_int_equal(5, typetag_length);
    assert_non_null(typetag);
    assert_string_equal(",ifsb", typetag);
    assert_int_equal(COSC_ESIZE, cosc_decode_typetag(&typetag, &typetag_length, osc_data, 7));
    assert_int_equal(COSC_ESIZE, cosc_decode_typetag(&typetag, &typetag_length, osc_data, 1));

    assert_int_equal(COSC_ESIZE, cosc_decode_typetag(&typetag, &typetag_length, osc_data, 4));
    assert_int_equal(COSC_ESIZE, cosc_decode_typetag(&typetag, &typetag_length, osc_data, 3));
}

static void test_decode_message_head(void **state)
{
    uint8_t osc_data[] =
        {'a', 'b', 'c', 'd', 0, 0, 0, 0,
         ',', 'i', 'f', 's', 'b', 0, 0, 0};
    const char *address = NULL, *typetag = NULL;
    int32_t address_length = 0, typetag_length = 0;

    assert_int_equal(16, cosc_decode_message_head(NULL, NULL, NULL, NULL, osc_data, 16));
    assert_int_equal(16, cosc_decode_message_head(&address, &address_length, &typetag, &typetag_length, osc_data, 16));

    assert_int_equal(4, address_length);
    assert_non_null(address);
    assert_string_equal("abcd", address);
    assert_int_equal(5, typetag_length);
    assert_non_null(typetag);
    assert_string_equal(",ifsb", typetag);

    assert_int_equal(COSC_ESIZE, cosc_decode_message_head(&address, &address_length, &typetag, &typetag_length, osc_data, 15));
    memcpy(osc_data, "#bundle", 8);
    assert_int_equal(COSC_EADDRESS, cosc_decode_message_head(&address, &address_length, &typetag, &typetag_length, osc_data, 15));
}

static void test_decode_message(void **state)
{
    uint8_t osc_data[128] = {0};
    memcpy(osc_data, "abcd", 4);
    memcpy(osc_data + 8, ",ifsbhtdScrmTFNI", 16);
    memcpy(osc_data + 28, (uint8_t[]){0x01, 0x02, 0x03, 0x04}, 4);
    memcpy(osc_data + 28 + 4, (uint8_t[]){0x42, 0xf6, 0xe9, 0x79}, 4);
    memcpy(osc_data + 28 + 8, "Yay", 3);
    memcpy(osc_data + 28 + 12, (uint8_t[]){0x00, 0x00, 0x00, 0x04}, 4);
    memcpy(osc_data + 28 + 16, (uint8_t[]){'B', 'L', 'O', 'B'}, 4);
    memcpy(osc_data + 28 + 20, (uint8_t[]){0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}, 8);
    memcpy(osc_data + 28 + 28, (uint8_t[]){0xff, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}, 8);
    memcpy(osc_data + 28 + 36, (uint8_t[]){0x40, 0x5e, 0xdd, 0x2f, 0x1a, 0x9f, 0xbe, 0x77}, 8);
    memcpy(osc_data + 28 + 44, "SYM", 3);
    memcpy(osc_data + 28 + 48, "C", 1);
    memcpy(osc_data + 28 + 52, (uint8_t[]){0x0ff, 0x02, 0x03, 0x04}, 4);
    memcpy(osc_data + 28 + 56, (uint8_t[]){0x081, 0x40, 0x7f, 0x00}, 4);

    union cosc_value values[16];
    values[0].i = 0x01020304;
    values[1].f = 123.456;
    values[2].s.ptr = "Yay";
    values[2].s.length = 3;
    values[3].b.ptr = "BLOB";
    values[3].b.length = 4;
    values[4].h = 0x0102030405060708;
    values[5].t = 0xff02030405060708;
    values[6].d = 123.456;
    values[7].S.ptr = "SYM";
    values[7].S.length = 3;
    values[8].c = 'C';
    values[9].r = 0xff020304;
    memcpy(values[10].m, (uint8_t[]){0x81, 0x40, 0x7f}, 3);

    const char *address = NULL, *typetag = NULL;
    union cosc_value tmp_values[16];
    int32_t num_values = 0, address_length = 0, typetag_length = 0;
    int data_size = 88;

    assert_int_equal(data_size, cosc_decode_message(NULL, NULL, NULL, NULL, NULL, 16, NULL, osc_data, sizeof(osc_data)));
    assert_int_equal(data_size, cosc_decode_message(&address, &address_length, &typetag, &typetag_length, tmp_values, 16, &num_values, osc_data, sizeof(osc_data)));
    assert_int_equal(15, num_values); num_values = 0;

    assert_non_null(address);
    assert_int_equal(4, address_length);
    assert_string_equal("abcd", address);
    assert_non_null(typetag);
    assert_int_equal(16, typetag_length);
    assert_string_equal(",ifsbhtdScrmTFNI", typetag);

    assert_int_equal(values[0].i, tmp_values[0].i);
    assert_float_equal(values[1].f, tmp_values[1].f, 0.0001);
    assert_non_null(tmp_values[2].s.ptr);
    assert_int_equal(values[2].s.length, tmp_values[2].s.length);
    assert_string_equal(values[2].s.ptr, tmp_values[2].s.ptr);
    assert_non_null(tmp_values[3].b.ptr);
    assert_int_equal(values[3].b.length, tmp_values[3].b.length);
    assert_memory_equal(values[3].b.ptr, tmp_values[3].b.ptr, tmp_values[3].b.length);
    assert_int_equal(values[4].h, tmp_values[4].h);
    assert_int_equal(values[5].t, tmp_values[5].t);
    assert_float_equal(values[6].d, tmp_values[6].d, 0.0001);
    assert_non_null(tmp_values[7].s.ptr);
    assert_int_equal(values[7].S.length, tmp_values[7].S.length);
    assert_string_equal(values[7].S.ptr, tmp_values[7].S.ptr);
    assert_int_equal(values[8].c, tmp_values[8].c);
    assert_int_equal(values[9].r, tmp_values[9].r);
    assert_memory_equal(values[10].m, tmp_values[10].m, 3);

    assert_int_equal(COSC_ESIZE, cosc_decode_message(&address, &address_length, &typetag, &typetag_length, tmp_values, 16, &num_values, osc_data, data_size - 1));
    assert_int_equal(COSC_ESIZE, cosc_decode_message(&address, &address_length, &typetag, &typetag_length, tmp_values, 16, &num_values, osc_data, 2));
}

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_decode_uint8),
        cmocka_unit_test(test_decode_int32),
        cmocka_unit_test(test_decode_uint32),
        cmocka_unit_test(test_decode_int64),
        cmocka_unit_test(test_decode_uint64),
        cmocka_unit_test(test_decode_float32),
        cmocka_unit_test(test_decode_float64),
        cmocka_unit_test(test_decode_string),
        cmocka_unit_test(test_decode_blob),
        cmocka_unit_test(test_decode_midi),
        cmocka_unit_test(test_decode_midi_args),
        cmocka_unit_test(test_decode_midi14),
        cmocka_unit_test(test_decode_bundle_head),
        cmocka_unit_test(test_decode_values),
        cmocka_unit_test(test_decode_typetag),
        cmocka_unit_test(test_decode_message_head),
        cmocka_unit_test(test_decode_message),
    };

    return cmocka_run_group_tests(tests, setup, teardown);
}
