/**
 * Copyright 2021 Peter Gebauer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>
#include <string.h>

#include "debug_utils.h"
#include "cosc.h"

const uint8_t OSC_DATA[] = {
    'a', 'b', 'c', 'd', 0, 0, 0, 0,
    ',', 'i', 'f', 's', 'b', 'h', 't', 'd', 'S', 'c', 'r', 'm', 'T', 'F', 'N', 'I', 0, 0, 0, 0,
    0x01, 0x02, 0x03, 0x04,
    0x42, 0xf6, 0xe9, 0x79,
    'Y', 'a', 'y', 0,
    0x00, 0x00, 0x00, 0x04, 'B', 'L', 'O', 'B',
    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
    0xff, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
    0x40, 0x5e, 0xdd, 0x2f, 0x1a, 0x9f, 0xbe, 0x77,
    'S', 'Y', 'M', 0,
    'C', 0, 0, 0,
    0x0ff, 0x02, 0x03, 0x04,
    0x081, 0x40, 0x7f, 0x00,
};


static int setup(void **state)
{
    return 0;
}

static int teardown(void **state)
{
    return 0;
}

static void test_find_offsets(void **state)
{
    int32_t ret;
    int32_t typetag_offset = 0, values_offset = 0, index_offset = 0;
    int32_t address_length = 0, typetag_length = 0;
    uint8_t osc_data[128] = {0};

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));

    ret = cosc_find_message_offsets(&address_length, &typetag_length, &typetag_offset, &values_offset, &index_offset, 0, osc_data, 3);
    assert_int_equal(COSC_ESIZE, ret);
    ret = cosc_find_message_offsets(&address_length, &typetag_length, &typetag_offset, &values_offset, &index_offset, 0, osc_data, 5);
    assert_int_equal(COSC_ESIZE, ret);
    ret = cosc_find_message_offsets(&address_length, &typetag_length, &typetag_offset, &values_offset, &index_offset, 0, osc_data, 10);
    assert_int_equal(COSC_ESIZE, ret);

    ret = cosc_find_message_offsets(&address_length, &typetag_length, &typetag_offset, &values_offset, &index_offset, 0, osc_data, sizeof(osc_data));
    assert_int_equal(88, ret);
    assert_int_equal(4, address_length);
    assert_int_equal(16, typetag_length);
    assert_int_equal(8, typetag_offset);
    assert_int_equal(28, values_offset);
    assert_int_equal(28, index_offset);

    typetag_offset = values_offset = index_offset = 0;
    address_length = typetag_length = 0;
    ret = cosc_find_message_offsets(&address_length, &typetag_length, &typetag_offset, &values_offset, &index_offset, 8, osc_data, sizeof(osc_data));
    assert_int_equal(88, ret);
    assert_int_equal(4, address_length);
    assert_int_equal(16, typetag_length);
    assert_int_equal(8, typetag_offset);
    assert_int_equal(28, values_offset);
    assert_int_equal(76, index_offset);
    assert_int_equal('C', osc_data[76]);

    ret = cosc_find_message_offsets(NULL, NULL, NULL, NULL, NULL, 0, osc_data, sizeof(osc_data));
    assert_int_equal(88, ret);
    ret = cosc_find_message_offsets(NULL, NULL, NULL, NULL, NULL, -1, osc_data, sizeof(osc_data));
    assert_int_equal(88, ret);
    index_offset = 0;
    ret = cosc_find_message_offsets(NULL, NULL, NULL, NULL, &index_offset, -1, osc_data, sizeof(osc_data));
    assert_int_equal(88, ret);
    assert_int_equal(-1, index_offset);
    ret = cosc_find_message_offsets(NULL, NULL, NULL, NULL, &index_offset, 16, osc_data, sizeof(osc_data));
    assert_int_equal(88, ret);
    assert_int_equal(-1, index_offset);
    ret = cosc_find_message_offsets(NULL, NULL, NULL, NULL, NULL, 0, osc_data, sizeof(OSC_DATA) - 1);
    assert_int_equal(COSC_ESIZE, ret);
    ret = cosc_find_message_offsets(&address_length, &typetag_length, &typetag_offset, &values_offset, &index_offset, 0, osc_data, 51);
    assert_int_equal(COSC_ESIZE, ret);
    ret = cosc_find_message_offsets(&address_length, &typetag_length, &typetag_offset, &values_offset, &index_offset, 0, osc_data, 41);
    assert_int_equal(COSC_ESIZE, ret);

    memset(osc_data, 0, sizeof(osc_data));
    ret = cosc_find_message_offsets(&address_length, &typetag_length, &typetag_offset, &values_offset, &index_offset, 0, osc_data, sizeof(osc_data));
    assert_int_equal(8, ret);
    osc_data[4] = 'a';
    ret = cosc_find_message_offsets(&address_length, &typetag_length, &typetag_offset, &values_offset, &index_offset, 0, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ETYPETAG, ret);
}

static void test_find_pointers(void **state)
{
    int32_t ret;
    const char *address = NULL, *typetag = NULL;
    int32_t num_values = 0;
    uint8_t osc_data[128] = {0};
    const void *pointers[128];

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_find_message_pointers(&address, &typetag, 0, 128, pointers, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA), ret);
    assert_int_equal(15, num_values);
    assert_ptr_equal(osc_data, address);
    assert_ptr_equal(osc_data + 8, typetag);

    assert_ptr_equal(osc_data + 28, pointers[0]);
    assert_ptr_equal(osc_data + 32, pointers[1]);
    assert_ptr_equal(osc_data + 36, pointers[2]);
    assert_ptr_equal(osc_data + 40, pointers[3]);
    assert_ptr_equal(osc_data + 48, pointers[4]);
    assert_ptr_equal(osc_data + 56, pointers[5]);
    assert_ptr_equal(osc_data + 64, pointers[6]);
    assert_ptr_equal(osc_data + 72, pointers[7]);
    assert_ptr_equal(osc_data + 76, pointers[8]);
    assert_ptr_equal(osc_data + 80, pointers[9]);
    assert_ptr_equal(osc_data + 84, pointers[10]);
    assert_ptr_equal(NULL, pointers[11]);
    assert_ptr_equal(NULL, pointers[12]);
    assert_ptr_equal(NULL, pointers[13]);
    assert_ptr_equal(NULL, pointers[14]);

    ret = cosc_find_message_pointers(NULL, NULL, 0, 0, NULL, NULL, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA), ret);
    ret = cosc_find_message_pointers(NULL, NULL, 1, 0, NULL, NULL, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA), ret);

    assert_int_equal(COSC_ESIZE, cosc_find_message_pointers(&address, &typetag, 0, 128, pointers, &num_values, osc_data, 7));
    assert_int_equal(COSC_ESIZE, cosc_find_message_pointers(&address, &typetag, 0, 128, pointers, &num_values, osc_data, 27));
    assert_int_equal(COSC_ESIZE, cosc_find_message_pointers(&address, &typetag, 0, 128, pointers, &num_values, osc_data, 31));
    assert_int_equal(COSC_ESIZE, cosc_find_message_pointers(&address, &typetag, 0, 128, pointers, &num_values, osc_data, 55));
    assert_int_equal(COSC_ERROR, cosc_find_message_pointers(&address, &typetag, -1, 128, pointers, &num_values, osc_data, sizeof(osc_data)));
    assert_int_equal(COSC_ERROR, cosc_find_message_pointers(NULL, NULL, 1000, 128, pointers, &num_values, osc_data, sizeof(osc_data)));
    osc_data[9] = 'x';
    assert_int_equal(COSC_ETYPETAG, cosc_find_message_pointers(&address, &typetag, 0, 128, pointers, &num_values, osc_data, sizeof(osc_data)));
    osc_data[8] = 'x';
    assert_int_equal(COSC_ETYPETAG, cosc_find_message_pointers(&address, &typetag, 0, 128, pointers, &num_values, osc_data, sizeof(osc_data)));
}

static void test_set_address(void **state)
{
    int32_t ret;
    uint8_t osc_data[128] = {0};
    int32_t address_length = 0;
    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_set_address("01234567", 1024, &address_length, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA) + 4, ret);
    assert_int_equal(8, address_length);
    ret = cosc_modify_set_address("01234567", 2, &address_length, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA) - 4, ret);
    assert_int_equal(2, address_length);
    ret = cosc_modify_set_address("#bundle", 8, &address_length, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_EADDRESS, ret);

    memcpy(osc_data, "#bundle", 8);
    ret = cosc_modify_set_address("01234567", 2, &address_length, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_EADDRESS, ret);
    ret = cosc_modify_set_address("01234567", 2, &address_length, osc_data, 4);
    assert_int_equal(COSC_ESIZE, ret);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_set_address("#bundl3", 8, &address_length, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA), ret);
    assert_memory_equal("#bundl3", osc_data, 8);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    const char *longstr = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    ret = cosc_modify_set_address(longstr, 128, &address_length, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, ret);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_set_address("", 0, NULL, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA) - 4, ret);
}

static void test_append_values(void **state)
{
    int32_t ret;
    uint8_t osc_data[128] = {0};
    int32_t num_values = 0;
    union cosc_value values[8] = {
        {.i=0x12345678},
        {.f=0.45},
        {.s={.ptr="DUH", .length=3}},
    };
    const uint8_t test_int[] = {0x12, 0x34, 0x56, 0x78};
    const uint8_t test_float[] = {0x3e, 0xe6, 0x66, 0x66};

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_append_values("ifs", 3, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA) + 12, ret);
    assert_int_equal(3, num_values);
    assert_memory_equal(test_int, osc_data + sizeof(OSC_DATA), 4);
    assert_memory_equal(test_float, osc_data + sizeof(OSC_DATA) + 4, 4);
    assert_memory_equal("DUH", osc_data + sizeof(OSC_DATA) + 8, 4);
    assert_memory_equal(",ifsbhtdScrmTFNIifs", osc_data + 8, 20);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_append_values(",ifs", 4, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA) + 12, ret);
    assert_int_equal(3, num_values);
    assert_memory_equal(test_int, osc_data + sizeof(OSC_DATA), 4);
    assert_memory_equal(test_float, osc_data + sizeof(OSC_DATA) + 4, 4);
    assert_memory_equal("DUH", osc_data + sizeof(OSC_DATA) + 8, 4);
    assert_memory_equal(",ifsbhtdScrmTFNIifs", osc_data + 8, 20);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    memset(values, 0, sizeof(values));
    ret = cosc_modify_append_values("iiii", 4, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA) + 20, ret);
    assert_int_equal(4, num_values);
    assert_memory_equal(",ifsbhtdScrmTFNIiiii", osc_data + 8, 21);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    memset(values, 0, sizeof(values));
    ret = cosc_modify_append_values("iiii", 0, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA), ret);
    ret = cosc_modify_append_values("iiii", 0, values, &num_values, osc_data, 32);
    assert_int_equal(COSC_ESIZE, ret);
    ret = cosc_modify_append_values("i", 1024, values, NULL, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA) + 4, ret);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    memset(values, 0, sizeof(values));
    ret = cosc_modify_append_values("x", 1, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ETYPETAG, ret);
}

static void test_insert_values(void **state)
{
    int32_t ret;
    uint8_t osc_data[128] = {0};
    int32_t num_values = 0;
    union cosc_value values[8] = {
        {.i=0x12345678},
        {.f=0.45},
        {.s={.ptr="DUH", .length=3}},
    };
    const uint8_t test_int[] = {0x12, 0x34, 0x56, 0x78};
    const uint8_t test_float[] = {0x3e, 0xe6, 0x66, 0x66};

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_insert_values(4, "ifs", 1024, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA) + 12, ret);
    assert_int_equal(3, num_values);
    assert_memory_equal(test_int, osc_data + 48, 4);
    assert_memory_equal(test_float, osc_data + 52, 4);
    assert_memory_equal("DUH", osc_data + 56, 4);
    assert_memory_equal(",ifsbifshtdScrmTFNI", osc_data + 8, 20);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_insert_values(4, ",ifs", 4, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA) + 12, ret);
    assert_int_equal(3, num_values);
    assert_memory_equal(test_int, osc_data + 48, 4);
    assert_memory_equal(test_float, osc_data + 52, 4);
    assert_memory_equal("DUH", osc_data + 56, 4);
    assert_memory_equal(",ifsbifshtdScrmTFNI", osc_data + 8, 20);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    memset(values, 0, sizeof(values));
    ret = cosc_modify_insert_values(4, "iiii", 4, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA) + 20, ret);
    assert_int_equal(4, num_values);
    assert_memory_equal(",ifsbiiiihtdScrmTFNI", osc_data + 8, 20);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    memset(values, 0, sizeof(values));
    ret = cosc_modify_insert_values(4, ",iiii", 0, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA), ret);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_insert_values(4, ",iiii", 0, values, &num_values, osc_data, 32);
    assert_int_equal(COSC_ESIZE, ret);
    osc_data[8] = 'i';
    osc_data[9] = 0;
    // cosc_dump_bytes(osc_data, 128);
    ret = cosc_modify_insert_values(4, ",i", 1, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ETYPETAG, ret);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_insert_values(-1, ",i", 2, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ERROR, ret);
    ret = cosc_modify_insert_values(15, ",i", 2, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ERROR, ret);
    ret = cosc_modify_insert_values(0, ",x", 2, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ETYPETAG, ret);
    values[0].s.ptr = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    values[0].s.length = 128;
    ret = cosc_modify_insert_values(0, ",s", 2, values, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ESIZE, ret);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_insert_values(0, ",T", 2, values, NULL, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA), ret);
}

static void test_remove_values(void **state)
{
    int32_t ret;
    uint8_t osc_data[128] = {0};
    int32_t num_values = 0;

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_remove_values(7, 100, &num_values, osc_data, sizeof(osc_data));
    // cosc_dump_bytes(osc_data, 64);
    assert_int_equal(sizeof(OSC_DATA) - 8 - 16, ret);
    assert_int_equal(8, num_values);
    assert_memory_equal(",ifsbhtd", osc_data + 8, 9);
    assert_memory_equal(OSC_DATA + 28, osc_data + 20, 44);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_remove_values(0, 4, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA) - 24, ret);
    assert_int_equal(4, num_values);
    assert_memory_equal(",htdScrmTFNI", osc_data + 8, 13);
    assert_memory_equal(OSC_DATA + 48, osc_data + 24, 40);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    ret = cosc_modify_remove_values(0, 4, &num_values, osc_data, 32);
    assert_int_equal(COSC_ESIZE, ret);
    ret = cosc_modify_remove_values(0, 0, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA), ret);

    ret = cosc_modify_remove_values(-1, 4, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ERROR, ret);
    ret = cosc_modify_remove_values(15, 4, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ERROR, ret);
    ret = cosc_modify_remove_values(-10, 4, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ERROR, ret);
    ret = cosc_modify_remove_values(100, 4, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ERROR, ret);
    
    osc_data[10] = 'x';
    ret = cosc_modify_remove_values(0, 1, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ETYPETAG, ret);

    osc_data[8] = 0;
    ret = cosc_modify_remove_values(0, 4, &num_values, osc_data, sizeof(osc_data));
    assert_int_equal(COSC_ETYPETAG, ret);

    memcpy(osc_data, OSC_DATA, sizeof(OSC_DATA));
    osc_data[24] = 'I';
    ret = cosc_modify_remove_values(15, 4, NULL, osc_data, sizeof(osc_data));
    assert_int_equal(sizeof(OSC_DATA), ret);
}

static void test_swap_endian(void **state)
{
    const uint8_t small_message[] = {
        '/', 0, 0, 0,
        ',', 'i', 'f', 'b', 'h', 't', 'd', 'r', 0, 0, 0, 0,
        0x01, 0x02, 0x03, 0x04,
        0x42, 0xf6, 0xe9, 0x79,
        0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00,
        0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
        0x08, 0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01,
        0x40, 0x5e, 0xdd, 0x2f, 0x1a, 0x9f, 0xbe, 0x77,
        0x08, 0x07, 0x06, 0x05
    };
    const uint8_t no_swaps[] = {
        '/', 0, 0, 0,
        's', 'S', 'c', 'm', 'T', 'F', 'N', 'I', 0, 0, 0, 0,
        'H', 'e', 'y', 0x00,
        'S', 'Y', 'M', 0x00,
        'A', 0x00, 0x00, 0x00,
        0x82, 0x40, 0x7f, 0x00,
    };
    int32_t ret;
    uint8_t tmpdata[1024] = {0};
    union cosc_value tmpvalue;
    memcpy(tmpdata, small_message, sizeof(small_message));
    ret = cosc_modify_swap_endian(tmpdata, sizeof(tmpdata));
    assert_int_equal(sizeof(small_message), ret);
    memcpy(&tmpvalue.i, tmpdata + 16, 4);
    assert_int_equal(0x01020304, tmpvalue.i);
    memcpy(&tmpvalue.f, tmpdata + 20, 4);
    assert_float_equal(123.456, tmpvalue.f, 0.001);
    memcpy(&tmpvalue.r, tmpdata + 24, 4);
    assert_int_equal(0x04, tmpvalue.r);
    memcpy(&tmpvalue.h, tmpdata + 32, 8);
    assert_int_equal(0x0102030405060708, tmpvalue.h);
    memcpy(&tmpvalue.t, tmpdata + 40, 8);
    assert_int_equal(0x0807060504030201, tmpvalue.t);
    memcpy(&tmpvalue.d, tmpdata + 48, 8);
    assert_float_equal(123.456, tmpvalue.d, 0.001);
    memcpy(&tmpvalue.r, tmpdata + 56, 4);
    assert_int_equal(0x08070605, tmpvalue.r);

    memcpy(tmpdata, small_message, sizeof(small_message));
    assert_int_equal(COSC_ESIZE, cosc_modify_swap_endian(tmpdata, sizeof(small_message) - 1));
    memcpy(tmpdata, small_message, sizeof(small_message));
    assert_int_equal(COSC_ESIZE, cosc_modify_swap_endian(tmpdata, 3));
    memcpy(tmpdata, small_message, sizeof(small_message));
    assert_int_equal(COSC_ESIZE, cosc_modify_swap_endian(tmpdata, 8));
    memcpy(tmpdata, small_message, sizeof(small_message));
    assert_int_equal(COSC_ESIZE, cosc_modify_swap_endian(tmpdata, 17));
    memcpy(tmpdata, small_message, sizeof(small_message));
    assert_int_equal(COSC_ESIZE, cosc_modify_swap_endian(tmpdata, 21));
    memcpy(tmpdata, small_message, sizeof(small_message));
    assert_int_equal(COSC_ESIZE, cosc_modify_swap_endian(tmpdata, 25));
    memcpy(tmpdata, small_message, sizeof(small_message));
    assert_int_equal(COSC_ESIZE, cosc_modify_swap_endian(tmpdata, 33));
    memcpy(tmpdata, small_message, sizeof(small_message));
    assert_int_equal(COSC_ESIZE, cosc_modify_swap_endian(tmpdata, 41));
    memcpy(tmpdata, small_message, sizeof(small_message));
    assert_int_equal(COSC_ESIZE, cosc_modify_swap_endian(tmpdata, 49));
    memcpy(tmpdata, small_message, sizeof(small_message));
    assert_int_equal(COSC_ESIZE, cosc_modify_swap_endian(tmpdata, 57));
    tmpdata[5] = 'x';
    assert_int_equal(COSC_ETYPETAG, cosc_modify_swap_endian(tmpdata, sizeof(tmpdata)));
    memcpy(tmpdata, no_swaps, sizeof(no_swaps));
    assert_int_equal(sizeof(no_swaps), cosc_modify_swap_endian(tmpdata, sizeof(tmpdata)));
    assert_memory_equal(no_swaps, tmpdata, sizeof(no_swaps));
    assert_int_equal(COSC_ESIZE, cosc_modify_swap_endian(tmpdata, 25));
}

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_find_offsets),
        cmocka_unit_test(test_find_pointers),
        cmocka_unit_test(test_set_address),
        cmocka_unit_test(test_append_values),
        cmocka_unit_test(test_insert_values),
        cmocka_unit_test(test_remove_values),
        cmocka_unit_test(test_swap_endian),
    };

    return cmocka_run_group_tests(tests, setup, teardown);
}
