/**
 * Copyright 2021 Peter Gebauer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "cosc.h"

static int setup(void **state)
{
    return 0;
}

static int teardown(void **state)
{
    return 0;
}

static void test_value_csize(void **state)
{
    assert_int_equal(sizeof(union cosc_value), cosc_value_csize());
}

static void test_strerr(void **state)
{
    const char *s;

    s = cosc_strerr(0);
    assert_non_null(s);
    assert_string_equal("", s);

    s = cosc_strerr(COSC_ERROR);
    assert_non_null(s);
    assert_string_equal("generic error", s);

    s = cosc_strerr(COSC_ENOMEM);
    assert_non_null(s);
    assert_string_equal("out of memory", s);

    s = cosc_strerr(COSC_EOVERFLOW);
    assert_non_null(s);
    assert_string_equal("integer overflow", s);

    s = cosc_strerr(COSC_ESIZE);
    assert_non_null(s);
    assert_string_equal("data size too small", s);

    s = cosc_strerr(COSC_ETYPETAG);
    assert_non_null(s);
    assert_string_equal("invalid typetag", s);

    s = cosc_strerr(COSC_EADDRESS);
    assert_non_null(s);
    assert_string_equal("invalid address", s);

    s = cosc_strerr(COSC_EPATTERN);
    assert_non_null(s);
    assert_string_equal("invalid pattern", s);

    s = cosc_strerr(-100);
    assert_non_null(s);
    assert_string_equal("unknown error", s);
}

#ifndef COSC_NOINT64

static void test_timetag_conversions(void **state)
{
    uint64_t timetag = 0x0000007b;
    timetag <<= 32;
    timetag |= 0x1f7ced91;

    assert_int_equal(timetag, cosc_seconds_to_timetag(123.123));
    assert_float_equal(123.123, cosc_timetag_to_seconds(timetag), 0.0001);

    assert_int_equal(UINT64_MAX, cosc_seconds_to_timetag(100000000000.0));
    assert_int_equal(0, cosc_seconds_to_timetag(-1.0));
    
    int64_t ts = 0, tn = 0;
    assert_int_equal(timetag + 1, cosc_timeval_to_timetag(123, 123000000LL));
    cosc_timetag_to_timeval(timetag, &ts, &tn);
    assert_int_equal(123, ts);
    assert_int_equal(122999999LL, tn);
    assert_int_equal(UINT64_MAX, cosc_timeval_to_timetag(10000000000LL, 0LL));
    assert_int_equal(0x100000000LL, cosc_timeval_to_timetag(1LL, 0LL));

    ts = 0;
    cosc_timetag_to_timeval(timetag, &ts, NULL);
    assert_int_equal(123, ts);
    tn = 0;
    cosc_timetag_to_timeval(timetag, NULL, &tn);
    assert_int_equal(122999999LL, tn);
}

static void test_normalize_timeval(void **state)
{
    int64_t ts = 0, tn = 0;

    cosc_normalize_timeval(0, 0, &ts, &tn);
    assert_int_equal(0, ts);
    assert_int_equal(0, tn);

    cosc_normalize_timeval(123, 456, &ts, &tn);
    assert_int_equal(123, ts);
    assert_int_equal(456, tn);

    cosc_normalize_timeval(0, 2500000000LL, &ts, &tn);
    assert_int_equal(2, ts);
    assert_int_equal(500000000LL, tn);

    cosc_normalize_timeval(0, -2500000000LL, &ts, &tn);
    assert_int_equal(0, ts);
    assert_int_equal(0, tn);

    cosc_normalize_timeval(-1, -2500000000LL, &ts, &tn);
    assert_int_equal(0, ts);
    assert_int_equal(0, tn);

    cosc_normalize_timeval(3, -2500000000LL, &ts, &tn);
    assert_int_equal(0, ts);
    assert_int_equal(500000000LL, tn);

    cosc_normalize_timeval(INT64_MAX, 1000000000LL, &ts, &tn);
    assert_int_equal(INT64_MAX, ts);
    assert_int_equal(999999999LL, tn);

    cosc_normalize_timeval(1, -500000000LL, &ts, &tn);
    assert_int_equal(0, ts);
    assert_int_equal(500000000LL, tn);

    cosc_normalize_timeval(0, -500000000LL, &ts, &tn);
    assert_int_equal(0, ts);
    assert_int_equal(0, tn);

    cosc_normalize_timeval(0, -1500000000LL, &ts, &tn);
    assert_int_equal(0, ts);
    assert_int_equal(0, tn);

    cosc_normalize_timeval(1LL, 1LL, &ts, NULL);
    assert_int_equal(1, ts);
    cosc_normalize_timeval(1LL, 1LL, NULL, &tn);
    assert_int_equal(1, tn);
}

#endif

static void test_is_bundle(void **state)
{
    char tmps[128] = {0};
    memcpy(tmps, "#bundle", 8);
    assert_true(cosc_is_bundle(tmps, 1024));
    assert_true(cosc_is_bundle(tmps, 20));
    assert_true(cosc_is_bundle(tmps, 19));
    assert_false(cosc_is_bundle("#bundlE", 20));
    assert_false(cosc_is_bundle("#bundle", 7));
}

static void test_typetag_find_invalid(void **state)
{
    assert_int_equal(COSC_ERROR, cosc_typetag_find_invalid("ifsbhtdScrmTFNI", 1024));
    assert_int_equal(COSC_ERROR, cosc_typetag_find_invalid(",ifsbhtdScrmTFNI", 1024));
    assert_int_equal(1, cosc_typetag_find_invalid("i,fsbhtdScrmTFNI", 1024));
    assert_int_equal(15, cosc_typetag_find_invalid("ifsbhtdScrmTFNIX", 1024));
    assert_int_equal(16, cosc_typetag_find_invalid(",ifsbhtdScrmTFNIX", 1024));
    assert_int_equal(1, cosc_typetag_find_invalid(",\n", 1024));
    assert_int_equal(COSC_ERROR, cosc_typetag_find_invalid("", 0));
}

static void test_address_find_invalid(void **state)
{
    assert_int_equal(COSC_ERROR, cosc_address_find_invalid("abcd", 1024));
    assert_int_equal(COSC_ERROR, cosc_address_find_invalid("abcd/efg", 1024));
    assert_int_equal(COSC_ERROR, cosc_address_find_invalid("abcd", 2));

    assert_int_equal(0, cosc_address_find_invalid("#", 1024));
    assert_int_equal(0, cosc_address_find_invalid("*", 1024));
    assert_int_equal(0, cosc_address_find_invalid(",", 1024));
    assert_int_equal(0, cosc_address_find_invalid("?", 1024));
    assert_int_equal(0, cosc_address_find_invalid("[", 1024));
    assert_int_equal(0, cosc_address_find_invalid("]", 1024));
    assert_int_equal(0, cosc_address_find_invalid("{", 1024));
    assert_int_equal(0, cosc_address_find_invalid("}", 1024));
    assert_int_equal(0, cosc_address_find_invalid("\t", 1024));
    assert_int_equal(0, cosc_address_find_invalid("\r", 1024));

    assert_int_equal(4, cosc_address_find_invalid("abcd#", 1024));
    assert_int_equal(4, cosc_address_find_invalid("abcd*", 1024));
    assert_int_equal(4, cosc_address_find_invalid("abcd,", 1024));
    assert_int_equal(4, cosc_address_find_invalid("abcd?", 1024));
    assert_int_equal(4, cosc_address_find_invalid("abcd[", 1024));
    assert_int_equal(4, cosc_address_find_invalid("abcd]", 1024));
    assert_int_equal(4, cosc_address_find_invalid("abcd{", 1024));
    assert_int_equal(4, cosc_address_find_invalid("abcd}", 1024));
    assert_int_equal(4, cosc_address_find_invalid("abcd\t", 1024));
    assert_int_equal(4, cosc_address_find_invalid("abcd\r", 1024));
}

static void test_address_match_plain(void **state)
{
    int32_t pattern_offset = -1;
    int32_t address_offset = -1;

    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, &address_offset, NULL, 1024, &pattern_offset));
    assert_int_equal(COSC_ERROR, cosc_address_match(NULL, 1024, &address_offset, "abcd", 1024, &pattern_offset));
    assert_int_equal(COSC_ERROR, cosc_address_match("a", 1024, &address_offset, "", 1024, NULL));
    assert_int_equal(COSC_ERROR, cosc_address_match("a", 1024, &address_offset, "", 0, NULL));
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "abcd", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, address_offset); address_offset = -1;
    assert_int_equal(7, cosc_address_match("abcdefg", 1024, &address_offset, "abcdefg", 1024, &pattern_offset));
    assert_int_equal(7, pattern_offset); pattern_offset = -1;
    assert_int_equal(7, address_offset); address_offset = -1;
    assert_int_equal(0, cosc_address_match("", 1024, &address_offset, "", 1024, &pattern_offset));
    assert_int_equal(0, pattern_offset); pattern_offset = -1;
    assert_int_equal(0, address_offset); address_offset = -1;

    assert_int_equal(COSC_ERROR, cosc_address_match("", 1024, &address_offset, "abcd", 1024, &pattern_offset));
    assert_int_equal(0, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, &address_offset, "", 1024, &pattern_offset));
    assert_int_equal(0, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, &address_offset, "abc", 1024, &pattern_offset));
    assert_int_equal(3, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, &address_offset, "abcdef", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, NULL, "abcdef", 1024, NULL));

    assert_int_equal(COSC_ERROR, cosc_address_match("abcdef", 1024, NULL, "abcd", 1024, NULL));
    assert_int_equal(COSC_ERROR, cosc_address_match("abcdef", 1024, NULL, "abcd", 3, NULL));

    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 3, NULL, "abcd", 1024, NULL));
}

static void test_address_match_question_mark(void **state)
{
    int32_t pattern_offset = -1;
    int32_t address_offset = -1;

    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "?bcd", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "a?cd", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "ab?d", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "abc?", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "?bc?", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "a??d", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "????", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;

    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, &address_offset, "X???", 1024, &pattern_offset));
    assert_int_equal(0, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, &address_offset, "?X??", 1024, &pattern_offset));
    assert_int_equal(1, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, &address_offset, "???X", 1024, &pattern_offset));
    assert_int_equal(3, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, &address_offset, "?XX?", 1024, &pattern_offset));
    assert_int_equal(1, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("", 1024, &address_offset, "?", 1024, &pattern_offset));

    assert_int_equal(COSC_ERROR, cosc_address_match("abcdef", 3, NULL, "????", 1024, NULL));
}

static void test_address_match_asterisk(void **state)
{
    int32_t pattern_offset = -1;
    int32_t address_offset = -1;

    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "*", 1024, &pattern_offset));
    assert_int_equal(1, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "a*", 1024, &pattern_offset));
    assert_int_equal(2, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "ab*", 1024, &pattern_offset));
    assert_int_equal(3, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "abc*", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "*d", 1024, &pattern_offset));
    assert_int_equal(2, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "*cd", 1024, &pattern_offset));
    assert_int_equal(3, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "*bcd", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "*bc*", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "a*d", 1024, &pattern_offset));
    assert_int_equal(3, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "*a**d*", 1024, &pattern_offset));
    assert_int_equal(6, pattern_offset); pattern_offset = -1;

    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, &address_offset, "x*", 1024, &pattern_offset));
    assert_int_equal(0, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, &address_offset, "*x", 1024, &pattern_offset));
    assert_int_equal(1, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("abcd", 1024, &address_offset, "ab*x", 1024, &pattern_offset));
    assert_int_equal(3, pattern_offset); pattern_offset = -1;

    assert_int_equal(COSC_ERROR, cosc_address_match("abcdef", 3, NULL, "*", 1024, NULL));
}

static void test_address_match_range(void **state)
{
    int32_t pattern_offset = -1;
    int32_t address_offset = -1;

    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "abc[a-d]", 1024, &pattern_offset));
    assert_int_equal(8, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "ab[a-d]d", 1024, &pattern_offset));
    assert_int_equal(8, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "ab[a-d]d", 1024, &pattern_offset));
    assert_int_equal(8, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "a[a-d]cd", 1024, &pattern_offset));
    assert_int_equal(8, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "[a-d][a-d][a-d][a-d]", 1024, &pattern_offset));
    assert_int_equal(20, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_EPATTERN, cosc_address_match("", 1024, &address_offset, "[", 1024, &pattern_offset));
    assert_int_equal(COSC_ERROR, cosc_address_match("z", 1024, &address_offset, "[a-d]", 1024, &pattern_offset));
    assert_int_equal(1, pattern_offset); pattern_offset = -1;

    assert_int_equal(COSC_EPATTERN, cosc_address_match("abcdef", 1024, NULL, "[1-", 1024, NULL));
    assert_int_equal(COSC_EPATTERN, cosc_address_match("abcdef", 1024, NULL, "[1-9]", 1, NULL));
    assert_int_equal(COSC_ERROR, cosc_address_match("0", 1024, NULL, "[1-8]", 1024, NULL));
    assert_int_equal(COSC_EPATTERN, cosc_address_match("0", 1024, NULL, "[1-8x", 1024, NULL));
}

static void test_address_match_sequence(void **state)
{
    int32_t pattern_offset = -1;
    int32_t address_offset = -1;

    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "abc[abcd]", 1024, &pattern_offset));
    assert_int_equal(9, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "ab[abcd]d", 1024, &pattern_offset));
    assert_int_equal(9, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "a[abcd]cd", 1024, &pattern_offset));
    assert_int_equal(9, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "[abcd]bcd", 1024, &pattern_offset));
    assert_int_equal(9, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "[abcd]bc[abcd]", 1024, &pattern_offset));
    assert_int_equal(14, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "[abcd][abcd][abcd][abcd]", 1024, &pattern_offset));
    assert_int_equal(24, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("a", 1024, &address_offset, "[x]", 1024, &pattern_offset));
    assert_int_equal(3, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_EPATTERN, cosc_address_match("a", 1024, &address_offset, "[a", 1024, &pattern_offset));
    assert_int_equal(2, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_EPATTERN, cosc_address_match("a", 1024, &address_offset, "[x", 1024, &pattern_offset));
    assert_int_equal(2, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_EPATTERN, cosc_address_match("abcdef", 1024, NULL, "[abcd", 2, NULL));
    assert_int_equal(1, cosc_address_match("a", 1024, NULL, "[a]", 2, NULL));
    assert_int_equal(COSC_EPATTERN, cosc_address_match("x", 1024, &address_offset, "[abc]", 3, &pattern_offset));
}

static void test_address_match_not_sequence(void **state)
{
    int32_t pattern_offset = -1;
    int32_t address_offset = -1;

    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "abc[!ABCD]", 1024, &pattern_offset));
    assert_int_equal(10, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "ab[!ABCD]d", 1024, &pattern_offset));
    assert_int_equal(10, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "a[!ABCD]cd", 1024, &pattern_offset));
    assert_int_equal(10, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "[!ABCD]bcd", 1024, &pattern_offset));
    assert_int_equal(10, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "[!ABCD]bc[!ABCD]", 1024, &pattern_offset));
    assert_int_equal(16, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "[!ABCD][!ABCD][!ABCD][!ABCD]", 1024, &pattern_offset));
    assert_int_equal(28, pattern_offset); pattern_offset = -1;
    assert_int_equal(-1, cosc_address_match("abcd", 1024, &address_offset, "[!]", 1024, &pattern_offset));
    assert_int_equal(3, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("a", 1024, &address_offset, "[!a]", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_EPATTERN, cosc_address_match("a", 1024, &address_offset, "[!", 1024, &pattern_offset));
    assert_int_equal(2, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_EPATTERN, cosc_address_match("a", 1024, &address_offset, "[!a", 1024, &pattern_offset));
    assert_int_equal(3, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_EPATTERN, cosc_address_match("x", 1024, &address_offset, "[!abc]", 3, &pattern_offset));
}

static void test_address_match_empty_sequence(void **state)
{
    int32_t pattern_offset = -1;
    int32_t address_offset = -1;

    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "abc[]d", 1024, &pattern_offset));
    assert_int_equal(6, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "ab[]cd", 1024, &pattern_offset));
    assert_int_equal(6, pattern_offset); pattern_offset = -1;
    assert_int_equal(4, cosc_address_match("abcd", 1024, &address_offset, "a[]bcd", 1024, &pattern_offset));
    assert_int_equal(6, pattern_offset); pattern_offset = -1;
}

static void test_address_match_single_strings(void **state)
{
    int32_t pattern_offset = -1;
    int32_t address_offset = -1;

    assert_int_equal(9, cosc_address_match("012345678", 1024, &address_offset, "{012}345678", 1024, &pattern_offset));
    assert_int_equal(11, pattern_offset); pattern_offset = -1;
    assert_int_equal(9, cosc_address_match("012345678", 1024, &address_offset, "012{345}678", 1024, &pattern_offset));
    assert_int_equal(11, pattern_offset); pattern_offset = -1;
    assert_int_equal(9, cosc_address_match("012345678", 1024, &address_offset, "012345{678}", 1024, &pattern_offset));
    assert_int_equal(11, pattern_offset); pattern_offset = -1;
    assert_int_equal(9, cosc_address_match("012345678", 1024, &address_offset, "{012}345{678}", 1024, &pattern_offset));
    assert_int_equal(13, pattern_offset); pattern_offset = -1;
    assert_int_equal(9, cosc_address_match("012345678", 1024, &address_offset, "{012}{345}{678}", 1024, &pattern_offset));
    assert_int_equal(15, pattern_offset); pattern_offset = -1;

    assert_int_equal(COSC_ERROR, cosc_address_match("abc", 1024, &address_offset, "{}", 1024, &pattern_offset));
    assert_int_equal(2, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("012345678", 1024, &address_offset, "{abc}345678", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("012345678", 1024, &address_offset, "012{abc}678", 1024, &pattern_offset));
    assert_int_equal(7, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("012345678", 1024, &address_offset, "012345{abc}", 1024, &pattern_offset));
    assert_int_equal(10, pattern_offset); pattern_offset = -1;

    assert_int_equal(COSC_EPATTERN, cosc_address_match("abc", 1024, &address_offset, "{", 1024, &pattern_offset));
    assert_int_equal(1, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_EPATTERN, cosc_address_match("abc", 1024, &address_offset, "{abc", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_EPATTERN, cosc_address_match("xyz", 1024, &address_offset, "{abc", 1024, &pattern_offset));
    assert_int_equal(4, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_EPATTERN, cosc_address_match("xyz", 1024, NULL, "{abc}", 1, NULL));
    assert_int_equal(COSC_EPATTERN, cosc_address_match("abc", 1024, NULL, "{abc}", 2, NULL));
    assert_int_equal(COSC_ERROR, cosc_address_match("abc", 2, NULL, "{abc}", 1024, NULL));
}

static void test_address_match_multi_strings(void **state)
{
    int32_t pattern_offset = -1;
    int32_t address_offset = -1;

    assert_int_equal(9, cosc_address_match("012345678", 1024, &address_offset, "{abc,xyz,012}345678", 1024, &pattern_offset));
    assert_int_equal(19, pattern_offset); pattern_offset = -1;
    assert_int_equal(9, cosc_address_match("012345678", 1024, &address_offset, "012{abc,345,xyz}678", 1024, &pattern_offset));
    assert_int_equal(19, pattern_offset); pattern_offset = -1;
    assert_int_equal(9, cosc_address_match("012345678", 1024, &address_offset, "012345{678,abc,xyz}", 1024, &pattern_offset));
    assert_int_equal(19, pattern_offset); pattern_offset = -1;
    assert_int_equal(9, cosc_address_match("012345678", 1024, &address_offset, "{abc,012}345{678,xyz}", 1024, &pattern_offset));
    assert_int_equal(21, pattern_offset); pattern_offset = -1;
    assert_int_equal(9, cosc_address_match("012345678", 1024, &address_offset, "{012,abc,xyz}{abc,345,xyz}{abc,xyz,678}", 1024, &pattern_offset));
    assert_int_equal(39, pattern_offset); pattern_offset = -1;

    assert_int_equal(COSC_ERROR, cosc_address_match("012345678", 1024, &address_offset, "{abc,xyz,klm}345678", 1024, &pattern_offset));
    assert_int_equal(12, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("012345678", 1024, &address_offset, "012{abc,xyz,klm}678", 1024, &pattern_offset));
    assert_int_equal(15, pattern_offset); pattern_offset = -1;
    assert_int_equal(COSC_ERROR, cosc_address_match("012345678", 1024, &address_offset, "012345{abc,xyz,klm}", 1024, &pattern_offset));
    assert_int_equal(18, pattern_offset); pattern_offset = -1;

    assert_int_equal(COSC_EPATTERN, cosc_address_match("abc", 1024, &address_offset, "{abc,xyz", 1024, &pattern_offset));
    assert_int_equal(8, pattern_offset); pattern_offset = -1;

    assert_int_equal(COSC_EPATTERN, cosc_address_match("xyz", 1024, NULL, "{abc,,xyz}", 6, NULL));
    assert_int_equal(COSC_EPATTERN, cosc_address_match("xyz", 1024, NULL, "{abcxyz}", 6, NULL));
}

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_value_csize),
        cmocka_unit_test(test_strerr),
        cmocka_unit_test(test_timetag_conversions),
        cmocka_unit_test(test_normalize_timeval),
        cmocka_unit_test(test_is_bundle),
        cmocka_unit_test(test_typetag_find_invalid),
        cmocka_unit_test(test_address_find_invalid),
        cmocka_unit_test(test_address_match_plain),
        cmocka_unit_test(test_address_match_question_mark),
        cmocka_unit_test(test_address_match_asterisk),
        cmocka_unit_test(test_address_match_range),
        cmocka_unit_test(test_address_match_sequence),
        cmocka_unit_test(test_address_match_not_sequence),
        cmocka_unit_test(test_address_match_empty_sequence),
        cmocka_unit_test(test_address_match_single_strings),
        cmocka_unit_test(test_address_match_multi_strings),
    };

    return cmocka_run_group_tests(tests, setup, teardown);
}
